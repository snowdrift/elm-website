---
title: Log in to Snowdrift.coop
type: page
---

# Log in to Snowdrift.coop

<login class="test">
</login>

Please note: a good way to generate a secure but memorable passphrase is
with a string of several random words. Also, never reuse the same
passphrase in multiple places. We recommend using a generator and a master
keyring.

For ensuring security in our system, we require your passphrase to have at
least nine characters.





 Don't have an account? [Sign Up!](/create-account)
