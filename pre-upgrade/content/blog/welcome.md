---
title: Welcome to the Snowdrift.coop blog
slug: welcome
date_published: 2014-11-24T18:41:00.000Z
date_updated: 2018-12-30T17:46:00.000Z
excerpt: Most of the writings here at Snowdrift.coop are on our [wiki pages](https://wiki.snowdrift.coop/). This blog serves a special function as a place for more personal writings from the team, timely writings that don't make sense for the wiki, and general update announcements…
---

Most of the writings here at Snowdrift.coop are on our [wiki pages](https://wiki.snowdrift.coop/). This blog serves as a place for more personal writings from the team, timely posts that don't make sense as wiki pages, and general update announcements. 

Subscribe to the blog via the [RSS feed](https://blog.snowdrift.coop/rss/).

For other project activity, see the [GitLab activity](https://git.snowdrift.coop/groups/sd/activity) and [community forum](https://community.snowdrift.coop/). You can also connect with us at [#snowdrift on IRC or Matrix](https://wiki.snowdrift.coop/community/irc).
