---
title: Come meet us at SeaGL 2019
slug: seagl-2019
date_published: 2019-10-31T00:49:00.000Z
date_updated: 2019-10-31T01:56:11.000Z
tags: Conferences
excerpt: Come meet us at SeaGL 2019, November 15-16 at Seattle Central College
---

![](/content/images/2019/10/logo-5.png)
Our next outreach event will be at [SeaGL 2019](https://seagl.org/), November 15-16 at Seattle Central College. Several of us have attended and spoken there before, but this is the first time we'll have a table in the main gathering room.

Besides our presence, the conference has a great schedule full of relevant talks including the following ones by members of the Snowdrift.coop team:

- [Codes of Conduct and Restorative & Transformative Justice](https://osem.seagl.org/conferences/seagl2019/program/proposals/706)
- As seen in how we structured our unique [Snowdrift.coop Code of Conduct](https://wiki.snowdrift.coop/community/conduct) and the associated [Values and Principles](https://wiki.snowdrift.coop/community/values)

- [Bicycles as a Metaphor for FLOSS](https://osem.seagl.org/conferences/seagl2019/program/proposals/690)
- [Glass Beatstation : An open source mobile and modular musical interface for Linux machines and musicians that don't know how to use Linux](https://osem.seagl.org/conferences/seagl2019/program/proposals/710)

## Brief status update

Since our last conference focus ([LFNW in April](https://blog.snowdrift.coop/lfnw-2019/)), we've made less visible progress than we hoped. However, we are getting better foundations in place for team recruitment, improved project management, and long-term sustainability. We hope to post more updates soon. If you care about our mission of better funding for public goods, you can help! Get involved by reaching out to us, such as participating at our forum: [https://community.snowdrift.coop](https://community.snowdrift.coop)
