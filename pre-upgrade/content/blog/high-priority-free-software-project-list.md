---
{  "title": "Snowdrift.coop applying to be a 2021 High Priority Free Software Project"
,  "slug": "high-priority-free-software-project-list"
,  "published": "2021-01-07"
,  "updated": "2021-01-24"
,  "description": "Since 2005, the FSF has been maintaining a list of High Priority Projects and there is currently an open call for submissions. We feel that Snowdrift.coop would make an excellent addition! We also feel that there is missing a key area: Sustainability and Growth. Read this post for more details..."
,  "type": "blog"
,  "author": "Snowdrift team"
,  "image": "images/article-covers/mountains.jpg"
,  "draft": false
}
---

Since 2005, the [Free Software Foundation](https://www.fsf.org/) (FSF) has been maintaining a list of [High Priority Projects](https://www.fsf.org/campaigns/priority-projects) (HPP), "a relatively small number of projects of great strategic importance to the goal of freedom for all computer users." In 2016, the HPP committee recommended [extensive updates to this list](https://www.fsf.org/blogs/community/a-preliminary-analysis-of-high-priority-projects-feedback). There is currently an [open call for submissions](https://www.fsf.org/blogs/community/help-us-set-high-priorities-for-2021-send-input-by-jan-8), closing on *January 8, 2021*.

We feel that Snowdrift.coop would make an excellent addition! We also feel that the HPP list is missing a key area: **Sustainability and Growth**. Therefore, we request that the FSF consider adding this area to the 2021 HPP list and including Snowdrift.coop within it.

### Summary

- A critical mass of community donors could provide the sustainable funding needed to enable the rest of the FSF high-priority projects
- Snowdrift.coop provides a viable solution with our innovative public-goods funding platform
- We are an exemplary fit for the HPP list and follow Free/Libre/Open (FLO) values *in every way we can*
- Our project fits many of the listed HPP categories, but we suggest a new one be added: *Sustainability and Growth*

## The Problem

The two business models that most effectively fund *end-user* software are **paywalls** and **advertisements**. Nobody *likes* those things; when bypassing them is trivial enough, most people are happy to do so. This creates an adversarial relationship between publishers and recipients: the need to restrict access and/or force people to see ads leads to proprietary licenses and DRM. Thus, paywalls and ads will never work well for sustaining end-user software freedom.

Some free software is funded by proprietary businesses… but this is almost always when the free software is used in the development of proprietary products. Rather than simply help our movement, this funding presents yet another conflict with the aims of software freedom.

Today, uncompromised free software relies on volunteer hours along with donations, grants, and government funding. Even in the most exceptional cases, end-user-focused free software lacks adequate resources and struggles to overcome the proprietary competition.

We must find practical ways for end users to fund software freedom on a much greater scale. Each individual donation only goes so far and the overall situation won't change unless **critical masses** of donors all work together. Solving this *coordination problem* (also known as the "[public goods dilemma](https://wiki.snowdrift.coop/about/snowdrift-dilemma)") is foundational for our movement, but is not trivial. Since we can't readily implement top-down schemes like Universal Basic Income or a massive increase in direct government funding, **we** must build stronger grass-roots **solidarity** in support of free software!

## Our Solution

Achieving effective mass coordination involves many different elements. A solution must be *pragmatic*, *sustainable*, and *accessible*. It must avoid conflicts-of-interest, it must minimize risks for participants, and it must adequately overcome the incentives to freeride — *without strictly blocking freeriding *(because that would mean proprietary lock-in).

Solving the problem of sustainable public goods is the **core mission** of Snowdrift.coop. Our name comes from the "[snowdrift dilemma](https://wiki.snowdrift.coop/about/snowdrift-dilemma)" — a game-theory variant of public goods dilemmas: *who will clear the snow from the public road?* Over several years[[1]](#footnote1), as yet another under-resourced volunteer free-software community, we have been shoveling what snow we can in order to prepare a better future for all public goods going forward.

Our primary focus has been the development of a new fundraising model: **crowdmatching**. On one hand, it is just another way to donate. On the other hand, it addresses the issue of coordination in a way no previous approach has achieved.

In crowdmatching, patrons make a monthly pledge where they donate *in proportion to the success of the overall campaign*. Instead of all or nothing (e.g. I give either $0 or $5 based on whether a goal is hit or not), crowdmatching pledges grow **with the crowd**. For example, if a sustainable goal for a project is half met, I'll give half of what I've pledged. Put another way, we each get to say, "I'm not willing to sacrifice all I can while everyone else freerides… but I **am** willing to be part of the crowd and part of the movement, *if* enough others help too!"

With crowdmatching, every additional patron helps the crowd reach toward the campaign goal, and thus, the whole crowd donates more; *everyone matches each other.* With ongoing monthly donations, crowdmatching combines *matching* and *sustaining memberships* — two of the most effective fundraising methods. It also avoids many pitfalls of all-or-nothing, make-or-break thresholds that are common for one-off campaigns (such as Kickstarter, etc).[[2]](#footnote2)

With ambitious goals, the potential impact of crowdmatching could be transformative for the whole free software ecosystem. It is also low risk: if very few patrons join a project, very little resources will be spent. Thus, we can escape the pattern where projects are kept going by relatively few dedicated donors and volunteers who struggle to carry the burden until they burn out.

## Why Snowdrift.coop?

Snowdrift.coop is a dedicated ally of software freedom. We are organizing as a non-profit dedicated to public goods and everything we produce gets published with FLO copyleft licensing. Our active volunteer team ([open to newcomers](https://wiki.snowdrift.coop/community/how-to-help)) covers a wide range of expertise and experience.[[3]](#footnote3) Additionally, we are dedicated to supporting a broad democratic movement that puts ethics and politics *front-and-center*.

All said, our implementation of crowdmatching is a means to the end of a more just society, rather than a project we develop out of mere technical interest. During the entirety of our existence, we have gone out of our way to avoid compromising our commitment to these ideals. We are also setting up our structure to eliminate any capacity to sell out our values later on.

Beyond that, the FSF has said that they are looking for:

> - Projects of great strategic importance to the goal of freedom for all computer users
> - Areas where people feel they are heavily pressured or even required to use proprietary software
> - Important missing features in existing free software, and for problems [...] on the horizon as technology is developing

As outlined above, Snowdrift.coop clearly fulfills the latter two requests. But what makes a project *"of great strategic importance to the goal of freedom for all computer users"*? To quote the volunteer HPP committee findings, "it takes one or more of the following characteristics":

> #### Systematic
> 
> Something that has the potential to improve lots of free software programs, development, communities, advocacy -- making use of free software and participation in development and advocacy more compelling for many more people.
> 
> #### Universal
> 
> Something that nearly every computer user needs, but for which there is no competitive free software in the category.
> 
> #### Cascading
> 
> Something that will enable a large number of users to replace not only comparable proprietary software, but also break a logjam that  makes it hard for users to adopt unrelated free software.
> 
> #### Frontier
> 
> Enables users to be free at ever lower layers of software and down to hardware. Few users may be able to do this soon, but such frontier development ensures that the bar for eventual freedom for all  users is not set too low.
> 
> #### Actionable
> 
> Such projects document ways for members of the free software community to get involved and make the project succeed, with any kind of concrete contributions, from money donation, to code patches, advocacy, etc.

Again, we hope to have conveyed that Snowdrift.coop fulfills *each and every one* of these characteristics.

## Where do we fit?

The current list of high priority free software areas include:

> - Free phone operating system
> - Decentralization, federation, and self-hosting
> - Free drivers, firmware, and hardware designs
> - Real-time voice and video chat
> - Encourage contribution by people underrepresented in the community
> - Free software and accessibility
> - Internationalization of free software
> - Security by and for free software
> - Intelligent personal assistant
> - Help GNU/Linux distributions be committed to freedom
> - Free software adoption by governments

Snowdrift.coop could make the case that it supports a number of these areas such as:

- decentralization (of funding) [...]
- (financial) contribution by people underrepresented in the community
- [...] commit[ment] to freedom (by relying upon user-centric funds)
- and all others, of course, by funding any projects in the other categories

However, we feel it would fit best in an important category which we think the HPP list is missing: **Sustainability and Growth**.

Funding, development, coordination, and longevity are all significant issues that FLO projects face, and they aren't easily solved. Other efforts in this category might focus on getting free software into schools, providing legal support and legislative lobbying for software freedom, or building alliances with compatible movements. Adding this category will emphasize the importance of a robust ecosystem within which software freedom can thrive. Regardless of category labels, we see sustainable funding as an important step toward realizing everything else on the HPP list.

## How the HPP will help?

Facing all the same dilemmas addressed above ourselves, the biggest obstacles to launching our platform are the limited time and energy of volunteers. Gaining new support and attention from aligned people who already appreciate our values could make all the difference to our success. The FSF is the birthplace of the Free Software Movement and continues to be one of its most steadfast champions. By promoting Snowdrift.coop on the High Priority Free Software Projects list, it will be lending its voice and support to amplify our message; and the success of Snowdrift.coop could make a huge difference in the overall success of the Free Software Movement!

## The Future

It will take still more time to coordinate enough people to either collectively shovel the snow and/or pool enough funds to pay for reliable snow clearing by professionals. The challenges have delayed our own launch greatly, but the importance of clearing the path is greater than ever. We are grateful for everyone who is simply taking the time to learn about the problems. Understanding the situation is the first step, and there is much more work that remains to be done after that.

Whether or not Snowdrift.coop is accepted as a 2021 HPP, we continue to push toward launch and a brighter world. If you (or others you know) might be interested in learning more or joining the effort, [please get in touch](https://snowdrift.coop/contact)!

---

[[1]]([1]): This journey has been much longer than any of us expected. Some of Aaron's (co-founder) feelings can be found in [this blog post](/crazy-ambition/). More history is available in other blog posts and on the [history wiki page](https://wiki.snowdrift.coop/about/history) which describes the initial founding and first couple years of the project.

[[2]]([2]): Read more on our wiki page discussing [threshold campaigns](https://wiki.snowdrift.coop/about/threshold-systems).

[[3]]([3]): We plan to finish an updated about-us web page with bios for the active team members. At this time, the best listing can be found in the [team group on our forum](https://community.snowdrift.coop/g/team).

---

*Title updated to reflect that this is an application, not an announcement of acceptance.*
