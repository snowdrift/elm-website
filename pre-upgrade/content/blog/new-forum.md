---
title: Announcing the Snowdrift.coop forum
slug: new-forum
date_published: 2018-12-18T22:54:29.000Z
date_updated: 2019-03-18T20:36:47.000Z
excerpt: We have a new(ish) forum at community.snowdrift.coop using Discourse, the popular Free/Libre/Open forum software (tweaked to fit our ideals).
---

Our new(ish) forum is live at [community.snowdrift.coop](https://community.snowdrift.coop)!

[![forum landing page image](/content/images/2018/12/forum-categories.png)](https://community.snowdrift.coop/)

We use [Discourse](https://www.discourse.org/), the popular Free/Libre/Open forum software. It has great features and flexibility. The only real downside: heavy reliance on client-side JavaScript (with NoScript, it's read-only).

## Using the forum

A quick overview of our setup:

At the Welcome category, newcomers can introduce themselves. We also have pinned posts there with other guidance for finding your way around and adjusting your preferences.

Within Announcements, we made an Events subcategory (by watching specific [geographic tags](https://community.snowdrift.coop/tags) you can get notified about regional meetups/conferences).

We discuss most of our work at the various "Clearing the Path" subcategories.

The forum itself has a lot of resources for learning your way around. Let us know in the Feedback & Support categories if you have *any* questions, suggestions, or other comments.

## Our Discourse setup tweaks

We made many adjustments to fit our ideals. Some highlights:

- SSO (Single Sign-On): forum users log in via our main Snowdrift.coop site
- [Ghost integration](https://docs.ghost.org/integrations/discourse/): This blog now mirrors to a [blog-posts forum category](https://community.snowdrift.coop/c/announcements/blog-posts) where each post automatically gets its own discussion topic
- ![](/content/images/2018/12/appreciate.png)The [new-like icon](https://meta.discourse.org/t/change-the-like-icon/87748) theme component changes the primary reaction from a heart to a thumb's-up; and we tediously customized all text references to change from "like" to "**appreciate**"[[1]](#fn1)
- ![](/content/images/2018/12/reactions.png)Added the [Retort plugin](https://meta.discourse.org/t/retort-a-reaction-style-plugin-for-discourse/35903) to offer [additional reactions](https://community.snowdrift.coop/t/using-the-extra-reaction-options/746)[[2]](#fn2)
- Changed Discourse's misnomer "FAQ" to "conduct" for our [Code of Conduct](https://community.snowdrift.coop/guidelines) as best as we could [[3]](#fn3)
- Tweaked preferences and text throughout

- For example, the default system message for a flagged post feels harsh, as if to reiterate the seriousness of a violation. Instead, we emphasize that flagging is just a chance to fix a problem and avoid exacerbating conflicts.[[4]](#fn4)

---

Read on for background history:

## Our indirect path to this point

Many of the sidetracks we've taken over the years came from the communications challenges for a team distributed around the world.

When we first considered tools in 2013, we looked into classic forums like phpBB (and the dozens of similar options), but those were lacking in several respects. Discourse was brand new, and although it has matured well in recent years, it didn't seem then to fit the direction we wanted.

### Our now-defunct integrated discussion boards

Seeing our analysis-paralysis, David (co-founder and tech-lead) just threw together a simple, threaded discussion board integrated with our main site. Our old discussion boards looked similar to Reddit or Hacker News, but we styled ours with better contrast, added avatar images next to posts, and more:

![image of old discussion-board style](/content/images/2018/12/old-discussion-boards-small.png)

Over time, we added features such as:

- An innovative flagging system [[5]](#fn5)
- A dialogue prompted the flagger to identify the specific Code of Conduct violation(s) and offered an option of adding constructive feedback
- The system automatically hid flagged posts, sent a message to the poster asking them to edit and fix the problem
- Fixed posts would be published cleanly (but notify the flagger so they could verify)

- A tagging system that allowed users to upvote and downvote the *tags* rather than the posts themselves
- A way to mark a post as "retracted" with an explanation as an alternative to deletion
- A way to close / collapse threads that were completed
- Posts turning into issue-tickets

- Via a key line in the post like "Ticket: Fix the spacing around the retraction notice", the post would automatically get a ticket number
- A separate page automatically listed all such posts across different discussion pages

- Moderation tools for moving posts to new locations

A good portion of the tickets we marked in that system were about improving the system itself. We had visions of making it easier for other projects on the site to use our integrated tools (and avoid all the hassle we went through…). **But we had fallen into producing discussion software instead of a funding platform.**

### Mailing lists

When Bryan took over development in 2015, he felt that the discussion system code was too intertwined and messy. Not only was it a distraction, but it made work on the core site harder.

After some debating of options, he set up mailing lists with GNU Mailman (version 2). We moved our issue tracking to outside tools.[[6]](#fn6) We backed up the old discussions boards and removed all the discussion stuff from our codebase.

The mailing lists were useful but did not fulfill all our needs. In general, mailing lists allow users to manage their own emails (what to keep, how to sort) while forums provide a centralized structure that everyone maintains together. Both approaches have pros and cons. We never used it, but Mailman 3 added a forum-like web-interface to have a more balanced approach. From the other side, Discourse is a forum first, but it has mailing-list-style options.

After we make our final announcements about the forum, we will shut down our mailing lists (though we plan to move the announcements list to CiviCRM). Thankfully, [mail-archive.com](https://www.mail-archive.com) retains public archives.

### Settling on Discourse

With Discourse, we can now edit, update, move posts, tag, organize, flag-and-hide conduct violations… Everyone can adjust their notifications on a per-topic, per-(sub)category, or per-tag basis…

Most of all, we get to be downstream of a robust, active project that *other* people work on — giving us more chance to focus on our core mission of building a better public-goods funding system.

We'll continue tweaking and updating our forum as we go. We'll also help upstream as much as we can. Maybe some day Discourse will use Snowdrift.coop for further funding…[[7]](#fn7)

---

1. 
We [discussed these decisions](https://community.snowdrift.coop/t/respect-and-agree-vs-like-and-other-ideal-reaction-options/483/5) using the forum itself, so you can read about our thoughts for more background. [↩︎](#fnref1)

2. 
If curious, see how we [decided on the reaction list](https://community.snowdrift.coop/t/custom-emojis-esp-as-retort-reactions/601). Note: to customize reactions the way we did takes two steps: 1. upload duplicate (or new) images in order to specify desired names, 2. adjust retort preferences to limit reactions to the desired set. [↩︎](#fnref2)

3. 
Bizarrely, Discourse uses "FAQ" and /faq for the built-in conduct guidelines. The default FAQ they offer is *not* questions and answers but is a general set of rules and conduct guidelines. Stranger still, they *used* to call it "guidelines". The workarounds we had to do: 1. add an *external* FAQ link in the settings — doing that brings /guidelines back to existence; 2. hide the FAQ link (we don't actually use it, though we could bring it back if we make an actual FAQ for the forum); 3. make custom navigation "Code of Conduct" links that go to /conduct which is itself an alias for /guidelines (and something they added today per our request!). [↩︎](#fnref3)

4. 
We'll only limit or ban anyone if they cannot or will not work to improve. Future blog posts will discuss more about our conduct policies and related issues. [↩︎](#fnref4)

5. 
Incidentally, Discourse is the only software we've seen that gets close to our vision of a great flagging system that adequately stops harms while helping everyone be their best. It's still not quite as good as our early design. We hope to someday describe the ideals clearly and inspire (or fund…) someone to make a Discourse plugin for our enhanced-flagging vision. [↩︎](#fnref5)

6. 
First to Open Project, later to Taiga, and then finally to GitLab; perhaps a topic for another blog post, another day… [↩︎](#fnref6)

7. 
Thankfully, they currently do at least alright with funding from their commercial support and hosting service. [↩︎](#fnref7)
