---
title: The New Snowdrift.coop Design
slug: new-design-2
date_published: 2015-09-15T23:22:00.000Z
date_updated: 2017-11-17T00:41:48.000Z
tags: Design
excerpt: After receiving the students' design, I started to improve whatever I could. I introduced use of the cartoon characters “Mimi & Eunice” by the amazing Nina Paley (whose work is the sort of free/libre/open culture we celebrate, so we can build upon it freely…). I also developed a new logo…
---

After receiving the [students' design](https://blog.snowdrift.coop/new-design-1), I started to improve whatever I could.

The biggest difference I introduced was making prominent use of the cartoon characters “[**Mimi & Eunice**](http://mimiandeunice.com/)” by the amazing [Nina Paley](http://ninapaley.com) throughout the whole page. After all, Nina's work is the sort of free/libre/open culture we celebrate, so we can build upon it freely, and it highlights our values.[[1]](#fn1) Aaron had already incorporated her comics in various pages such the one on [*Non-Scarce Economics*](https://wiki.snowdrift.coop/about/economics). I just embraced the characters fully to create a more engaging and consistent design.

![](/content/images/2017/05/me.png)

---

I also switched the main font to “[Nunito](https://www.google.com/fonts/specimen/Nunito)” which was introduced by earlier style tiles from the students.

## New logo

Unfortunately, the student team had no time to address the logo as part of their relaunch, so I jumped in and created a quick alternative. That first draft didn't really meet my standards and soon got replaced by a more promising one that better in many respects but *still* not good enough. So, I devoted substantial time to get a dynamic, unique, simple, scalable, reproducible and fitting logo:

![](/content/images/2017/05/logo.png)

## Iterative design process

To get an idea of the whole design process, here's a video slide-show from my first draft building on the student work to the current final homepage design:

Your browse dos not support html5 video in webm format.

## Applying the new design

Just the welcome page isn't enough, of course. In order to cover a broader set of design elements, I applied the new look across [multiple pages](https://github.com/mray/Snowdrift-Design/tree/master/mray%20website%20mockups%20). From those pages, I derived the basic underlying design decisions and put together our new [**design guide**](https://wiki.snowdrift.coop/design/design-guide). This makes new contributions and new pages faster and easier to stay consistent with our identity.

## Come help us implement!

Now, the design needs to be hammered into a responsive webpage. If you happen to like working with HTML, CSS, and Boostrap, please come help us! Join our live chat on IRC at [#snowdrift](https://wiki.snowdrift.coop/community/irc) and our [design email list](http://lists.snowdrift.coop/mailman/listinfo/design), and consider filling out a volunteer form. Of course, we always welcome any other kind of support, too. The new theme is just *one* important element. Other areas to help with include general illustration, back-end coding in Haskell, help with welcoming other volunteers, legal research… See our [how-to-help page](https://wiki.snowdrift.coop/community/how-to-help) for more details.

---

1.
[Nina](/u/16) was also the first non-founding member of the Snowdrift.coop steering committee back in early 2013, though she's currently too busy for formal involvement. [↩︎](#fnref1)
