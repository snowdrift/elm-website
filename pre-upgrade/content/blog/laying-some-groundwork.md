---
title: Laying Some Groundwork
slug: laying-some-groundwork
date_published: 2015-05-26T00:05:00.000Z
date_updated: 2017-11-10T22:02:54.000Z
excerpt: Continuing on last month's article, I will here include things from my technical realm as well as from other parts of the project. As you will see, a lot has been done to give a firmer foundation for the project including conferences, volunteer-intake, and technical guides…
---

chreekat the lead dev, checking in!

I wrote [an article](https://blog.snowdrift.coop/greetings-from-new-lead-dev) last month where I introduced myself and gave a high-level survey of the state of Snowdrift.coop technology. Now that my understanding has deepened, I can go into more details about recent accomplishments, current work, and future areas of effort. For this general post, I will include things from my technical realm as well as from other parts of the project. As you will see, a lot has been done to give a firmer foundation for the project.

---

## Better volunteer intake

Snowdrift.coop has always striven to be welcoming to volunteers of all levels, and now we have greatly expanded our instructions for beginners. [Peter Harpending](/u/759) kicked off [BEGINNERS.md](https://git.gnu.io/snowdrift/snowdrift/blob/master/BEGINNERS.md), and with substantial editing from [Aaron](/u/3) and others, it has morphed into a thorough guide for anyone getting started with the code.

We have, in fact, a whole menagerie of guides now. The standard [README.md](https://git.gnu.io/snowdrift/snowdrift/blob/master/README.md) is now short and simple. The full [GUIDE.md](https://git.gnu.io/snowdrift/snowdrift/blob/master/GUIDE.md) has deeper details, and the new [SETUP_DEBIAN.md](https://git.gnu.io/snowdrift/snowdrift/blob/master/SETUP_DEBIAN.md) has a concise set of instructions for volunteers running Debian and Ubuntu.

[Stephen Paul Weber](/u/351) introduced instructions for using Vagrant in [SETUP_VAGRANT.md](https://git.gnu.io/snowdrift/snowdrift/blob/master/SETUP_VAGRANT.md). This tool provides a consistent build environment for all platforms — even users of OS X and Windows can now get hacking on Snowdrift with minimal fuss!

These documents are still being constantly improved, as ideas and collaborations filter throughout the Haskell ecosystem.  Maybe we've got something useful for your own project in here?

## Conferences and expos

Volunteers [Denver Bohling](/u/335) and [Sam Erbs](/u/339) joined Aaron and me in running our booth at [LinuxFest Northwest](http://linuxfestnorthwest.org/2015) last month. Although that was personally my first event with Snowdrift.coop, Aaron was at [LibrePlanet](https://libreplanet.org/2015/) in March, and in February Aaron and co-founder [David Thomas](/u/1) managed Snowdrift.coop's first expo booth for [SCALE](http://www.socallinuxexpo.org/scale/13x).

Special thanks to the organizers of LinuxFest Northwest and SCALE for believing in our mission enough to invite us to these events. We've made great connections and met many new supporters.

Looking ahead, we'll have a Snowdrift hacking table at [BayHac 2015](http://bayhac.org/) next month.  The following week, Aaron will be giving a [talk at Open Source Bridge](http://opensourcebridge.org/sessions/1598) about community building and bringing in less-technical volunteers. I'll be at OSB as well. If any of you will be attending any of these events, let us know!

## Technical news

In the technical realm, I have focused on contributor engagement and on developing the funding mechanism. Others have made progress on clarifying the user experience.

### Mailing lists and notifications

*We finally have mailing lists!* Head on over to our [mailing lists](http://lists.snowdrift.coop/) page to get your hot, fresh list subscriptions.

We also now have better defaults for project notifications, thanks to contributions by Aaron and [Nikita Karetnikov](/u/319). Along with mailing lists, notifications give Snowdrift.coop better ways to keep interested people aware of site developments, and lets us follow up with all the connections we've made at events. More notification improvements are on the way.

### Transition to git.gnu.io

In a surprisingly painless transition, we moved from the now-defunct gitorious.org to our new home on [git.gnu.io](https://git.gnu.io/snowdrift/snowdrift). We still have a Github account, but as champions of [FLO](/p/snowdrift/w/en/free-libre-open) values, we're happy to be contributing to [*cough* stress-testing] this new service that is backed by free software.

### Design and development

[Robert Martinez](/u/726) continues to ask cutting, incisive questions about Aaron Wolf's vision for the site, leading to a new world of clarity in design. Who know you could have a two-week discussion over what, exactly, a single button is supposed to do? ;) Incidentally, the design and UX efforts being led by Robert are building off of draft work supplied by local community college students (see Aaron's mention in an [earlier blog post](https://blog.snowdrift.coop/transitions-2015-02#site-design-transition)).

At the same time, Nikita has been producing tests for the properties the formula should have. As a result of all of these great contributions, the [formula](https://snowdrift.coop/p/snowdrift/w/en/formula) has been modified to a simpler form. Some corner cases [still remain](https://git.gnu.io/snowdrift/snowdrift/merge_requests/16), but the combination of better testing and better vision is producing nice results. I chipped in too on the back-end, implementing part of the [mechanism](/p/snowdrift/w/en/mechanism) pertaining to dropping underfunded shares.

Having clean code facilitates participation, so I started cleaning up the code and updating it to our [code style guidelines](/p/snowdrift/w/en/coding#code-style-guide). I suspect we'll be doing a lot of refactoring in the coming weeks, including no small amount of reformatting [long code lines](https://git.gnu.io/snowdrift/snowdrift/commit/c5c64a7dfb969f70c078dd04d1ae314a8936a339).

## Looking ahead

Future plans are still hard for me to pin down. I consider contributor involvement tools and the funding mechanism to be the highest development priorities, but as lead dev and architect I also have a strong need for clarity on other collaborators' priorities. I began taking notes on all our [400+ open tickets](/p/snowdrift/t), and some development work to manage them all may serve the project well.

It goes without saying that there is a ton of stuff to do, and I will be working hard to make it understandable and approachable to contributors of all ability levels (myself included). Until next time, I'll see you on [IRC](/p/snowdrift/w/en/irc)!
