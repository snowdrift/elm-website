---
title: New Design on Its Way
slug: new-design-1
date_published: 2015-07-14T16:51:00.000Z
date_updated: 2017-11-10T21:58:03.000Z
tags: Design
excerpt: Hi there! With this first post from the design department, I won't talk too much about me but rather present progress so far. …a group of design students create a rather detailed design brief, and two different style tiles…
---

Hi there! With this first post from the design department, I won't talk too much [about me](https://wiki.snowdrift.coop/community/team/mray) but rather present progress so far:

By the time I joined the Snowdrift.coop team, [Aaron](https://wiki.snowdrift.coop/community/team/wolftune) had worked with a group of design Students (Charles Allen, Taylor Keckler, [Cody Johnson](http://www.designbypixl.com) and Nathan Laughlin) to create a rather detailed [design brief](https://wiki.snowdrift.coop/blog/assets/design-brief-pdf.pdf). Based on that document, the team built two different [style tiles](http://styletil.es/) to find a new look for the current page that was created by project members with minimial webdesign experience:

---

## Current Page

[![Screenshot of the current page](/content/images/2017/04/Status-Quo.png)](https://snowdrift.coop/)

## New Style Tiles

### Style tile #1

[![Style Tile 1](/content/images/2017/04/Style-Tile-1.png)](https://git.snowdrift.coop/sd/design/blob/master/blog-posts/2015-09-15-new-design-mray/style-tile1-pdf.pdf)

### Style tile #2

[![Style Tile 2](/content/images/2017/04/Style-Tile-2.png)](https://git.snowdrift.coop/sd/design/blob/master/blog-posts/2015-09-15-new-design-mray/style-tile2-pdf.pdf)

The community feedback generally favored style tile #1, and after incorporating our feedback the design students handed over their final work in CSS and HTML as their class term ended (click the image to download the actual code):

## Final CSS and HTML

[![The final version of the students work in CSS and HTML](/content/images/2017/04/HTML-CSS.png)](https://git.snowdrift.coop/sd/design/blob/master/blog-posts/2015-09-15-new-design-mray/html-css.zip)

The students gave us a great starting point:  a color palette, a set of fonts, and a reference implementation. Being a designer myself, I'm developing on top of that foundation, addressing unsolved issues with content, navigation, logo, typography, and the illustration.

## Content

In order to get a real impression of design work we still needed, I started to create some [wireframes](https://en.wikipedia.org/wiki/Website_wireframe) of the most vital pages. It quickly dawned on me that simple wireframes were not enough. Snowdrift.coop needs many different pages that don't even remotely resemble what they look like right now. So, instead of communicating and constantly updating lots of wireframes, I decided to create a [clickable prototype](https://mray.github.io/snowdrift-prototype/) (using [bootstrap](http://getbootstrap.com/) and [jekyll](http://jekyllrb.com/) on [github.io](github.io)).

[![Prototype](/content/images/2017/04/Prototype.png)](https://mray.github.io/snowdrift-prototype/)

At this point, I still need more feedback in order to improve my suggested layout and navigation system. Please consider posting your thoughts in the discussion here on this blog or on [IRC](https://kiwiirc.com/client/irc.freenode.net/#snowdrift) or the [design email list](https://lists.snowdrift.coop/mailman/listinfo/design). In the mean time, I've been working on the homepage illustration and logo — more on that in the next blog post…
