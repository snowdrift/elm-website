---
title: We're still here, more news coming soon…
slug: still-here
date_published: 2017-11-10T19:35:08.000Z
date_updated: 2017-11-15T02:53:53.000Z
excerpt: It's been five years, have we got the road cleared by now? When will Snowdrift.coop finally launch? Is the project still active? I want this to happen! Let me pledge and give you money!…

I've heard these types of questions for a while now. I've usually answered like this:
---

> It's been five years, have we got the road cleared by now? When will Snowdrift.coop finally launch? Is the project still active? I want this to happen! Let me pledge and give you money!

I've heard these types of questions for a while now. I've usually answered like this:

> We're getting there. I wish I could say it will launch this year, but I don't know. We're still active — look at the [GitLab activity](https://git.snowdrift.coop/groups/sd/activity), [mailing list archives](https://lists.snowdrift.coop/), and [IRC/Matrix history](https://riot.im/app/#/room/#snowdrift:matrix.org). I want this too, and actually we turned on real pledging a while ago but still haven't gotten around to announcing it (wanting certain things in place first)…

I know that's not quite what people want to hear, but they're always happy that things are better than they appear. But what's taken so long? What challenges remain?

Well, thanks to Iko and Salt (two of our volunteer team members), we finally have this new blog running [Ghost](https://ghost.org/). We will get back on track with blog posts now with a series of posts coming soon![[1]](#fn1)

---

1. 
For reference, the original blog was integrated into Snowdrift.coop itself with the promise of offering a built-in blog for any project that wanted one. Our blog system used no JavaScript, had associated discussion pages, and was simple and clean… But the code was too intertwined with the rest of the site functions. In spring 2016, Bryan, our lead developer, decided to strip out all the non-essential features in order to get a better solid foundation for the platform. We temporarily moved all the blog posts to our new, now-separated [wiki](https://wiki.snowdrift.coop) and now finally have moved to the new permament blog.snowdrift.coop location. While I could go on about the pros and cons of Ghost and of integrated monolithic solutions, I'm just happy to have a good blog tool back in operation. [↩︎](#fnref1)
