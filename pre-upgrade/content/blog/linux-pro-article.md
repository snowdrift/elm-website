---
title: Interview for Linux Magazine
slug: linux-pro-article
date_published: 2014-11-24T19:13:00.000Z
date_updated: 2017-11-17T05:15:27.000Z
excerpt: Bruce Byfield, a long-time software freedom advocate and journalist, contacted me last month to do an interview for Linux Magazine. He took the interview and produced an excellent Linux-Magazine.com article on Snowdrift.coop…
---

Bruce Byfield, a long-time software freedom advocate and journalist, contacted me last month to do an interview for Linux Magazine. He took the interview and produced an excellent [Linux-Magazine.com article on Snowdrift.coop](http://www.linux-magazine.com/Online/Features/Snowdrift.coop) which will also go in the print edition of Linux Pro #170.

![LMI_plus_DVD_170_600x646_issue_small](/content/images/2017/11/LMI_plus_DVD_170_600x646_issue_small.png)

Incidentally, I hadn't read Bruce's work before, but I took the chance now and am very impressed. I highly recommend all of [Bruce's editorials](http://www.linux-magazine.com/Online/Blogs/Off-the-Beat-Bruce-Byfield-s-Blog) to everyone interested in GNU/Linux and software freedom!

While Bruce's article offers a great concise summary, we're posting here the full interview as it provides a lot of extra material and in a somewhat different style from what we have elsewhere on the site.

---

### Give some background about yourself, particularly your involvement with technology and free software.

As a music teacher by trade, I first became interested in Free/Libre/Open (FLO) issues regarding copyright, music sharing, and technology in the music business and music education in particular.

Over the years, I used some FLO tools on my Mac, such as Audacity and VLC. Although many factors contributed to my growing distrust in Apple, I remember feeling outraged when I learned that Apple's iOS terms effectively censor GPL software. Volunteers worked to provide valuable free resources to the world and then Apple could just block users' access in order to compel people to get only proprietary apps and see ads or pay.

In early 2012, I finally tried a GNU/Linux system, and the welcoming community far surpassed my expectations. It was a shock to interact personally with developers given my prior computing experience as only a disconnected end-user of mostly corporate products. I felt especially baffled about so much being free-of-charge and wondered, "who pays for the servers for hosting all these software updates?"

As I got more involved, I felt the need to pay forward the assistance others had given me when I got started. I helped write documentation for KXStudio, the premier GNU/Linux music system (available as add-on repositories to any Debian-based distro), and I joined the team working on Task Coach, perhaps the most robust FLO personal task manager. As a non-programmer, I contributed by sorting feature requests and bug reports, answering support questions, organizing development plans, documentation, and so on.

I introduced some of my music students to KXStudio, but various rough spots and issues made it quite a challenge. These systems have immense potential, but the FLO world still lacks the polish of the proprietary competition in most cases.

I still teach music lessons, but now much of my time goes to Snowdrift.coop. I'm even studying Haskell so I can understand and help a little with the code here and there.

### Describe the idea of Snowdrift.coop and the Snowdrift dilemma

We face basic economic dilemmas when it comes to funding public works. After a big snowstorm, everyone wants the roads cleared, but nobody wants to try to clear the road alone or pay the entire cost themselves. We need a critical mass of neighbors to cooperate together.

Today, tax dollars pay for the clearing of most roads, so everyone is compelled by law to chip in their part. Without taxes, we mostly only get toll roads. Tolls put the burden on those who most use the resource, but they also cause an artificial impediment that actually lowers the overall value of the road. The problem is: few would pay if we make it purely voluntary — even though everyone wants the work done.

Here's the full game theory version: two players have to choose whether to help clear a snowdrift. If nobody helps, nobody gets any points (the road remains blocked). If you get the other player to do it all themselves, you get 300 points (a clear road at no cost to you), and they get 100 points (a clear road at heavy cost). If you both help, you each get 200 points (clear road, modest cost). Of course, if both players try to freeride, both get nothing. So it makes sense to cooperate. The problem is: if one player goes out to shovel first, then the other has less incentive to help. So, in this game, nobody wants to go first, and whoever has to get through sooner ends up doing it alone.

Of course, the particular payoffs and issues vary depending on context. Often situations are more like the even more intractable Prisoner's Dilemma. Real life is more complex than game theory (some people enjoy shoveling snow, for example), but it's a useful framework to consider. We've written more about the game theory at [https://wiki.snowdrift.coop/about/snowdrift-dilemma](https://wiki.snowdrift.coop/about/snowdrift-dilemma).

This dilemma explains why plain donation buttons are not that effective. For each new user  asked to donate, the existing donors are a given. The project may be doing ok or may be struggling, but, either way, a new user's small donation makes basically no difference. If they bother at all, they'll donate just to the extent it makes them feel good.

Snowdrift.coop is a platform designed to solve this dilemma. We're coordinating donors to better invite and incentivize new donors to join while reinforcing the ongoing commitments from current donors. We do this with a simple pledge: "I'll chip in a sustaining share based on how many others are with me in supporting this project — I'll do more if more people will help."

Our proposed minimum share to be a patron of a project is 1/10 of a cent per month per the number of total patrons. If everyone pledges at that minimum, it means $1 per patron at 1,000 patrons ($1,000 total) or $5 per patron at 5,000 patrons ($25,000 total), and so on. Also, stronger (or wealthier) supporters can make larger pledges.

Each patron sets an overall budget based on how much funds they make available to the system. If a patron runs out of funds in their account, their pledges just get dropped (become inactive). So, the share value can never reach absurdly high levels unless everyone involved truly wants to keep funding and actually puts up enough funds. I'm not going to just donate my life-savings unilaterally, but I certainly would do it if I knew the rest of the world would join me to fundamentally change the economy for the better if and only if I donated all I could.

The writings on the site discuss various details further, such as the idea of eventually offering institutional share-levels so that larger entities can match each other using the same system.

We're also integrating a wide range of feedback and volunteer mechanisms because projects thrive on community support in ways that are independent of money. We might even offer a similar pledge for volunteer time as for financial donations, but quantifying time and work is not as simple.

Overall, our system combines the mutual-assurance of threshold goals and matching-pledges with the accountability and reliability of sustaining memberships. Because funding is spread out over time, patrons can increase or reduce their support of projects to favor the ones that do the best work.

### How did Snowdrift.coop begin? What influenced it? Was there a moment when the idea was born, or did it gradually emerge from other things?

I proposed a one-time version of this matching idea when I was trying to figure out how to  help fund Task Coach. The developers weren't sure how to implement it, and that's when they convinced me to volunteer my time instead. I had the same hesitation about volunteering my time unilaterally as I had about donating unilaterally, but I was interested in learning more about how it all worked.

Later, I was chatting with my friend, David Thomas, and told him about my experience getting involved with Task Coach. He liked the matching-donation idea and offered to work with me to build the infrastructure to support it. But I didn't want to put up yet another random website to clutter the field. David had to work hard to convince me that this was truly worth doing. When I decided to do it, I went all-in, working to get every detail right and thoroughly research all the relevant issues.

### What stage is the project at now?

About two years after initializing, we now have a functioning test site; a first full draft of bylaws; many wiki pages of writings covering background research, explanations, and project plans; and a community of volunteers and supporters enthusiastic about making this happen.

Our progress has been somewhat slow because we're a bootstrapped non-profit cooperative. In essence, we're facing the same issues that many projects face. We work with the limited time and resources we have. As we get the system running, we will use it ourselves to accelerate our progress and hopefully become sustainable long-term.

In the near future, we plan to run a one-time fund-drive to cover legal costs and other expenses toward finally launching. As of this writing, the drive isn't live yet, but it will be soon at snowdrift.tilt.com [update 2017: Tilt shut down so the old campaign site is gone].

### Why the emphasis on free / open stuff

We're addressing a public goods dilemma. It doesn't apply to proprietary products which already have their business model: pay-for-access (whether paying in dollars or via attention to ads). Our solution is for a problem specific to FLO projects.

Additionally, I don't like having a world full of proprietary restrictions and obnoxious advertising. We're motivated by our mission to help the FLO world specifically and to put an end proprietary anti-features.

Finally, we provide a great service to the community by having a curated platform where you don't have to check the license details — you just know that everything on the site is FLO.

I wrote a longer essay about this issue at [https://wiki.snowdrift.coop/about/why-flo](https://wiki.snowdrift.coop/about/why-flo)

### What problems make up the dilemma, and how will Snowdrift.coop approach or solve them?

Most of the problems in the FLO world are all intertwined. Lack of funding relates to rough product quality which translates to lower user adoption. Another problem is the developer-centric nature of FLO software. The most advanced FLO software serves upstream purposes for companies who still make proprietary final products; and downstream, user-facing FLO programs are understandably biased toward the interests of their volunteer programmers rather than being designed to best serve the general public. Aral Balkan (of [http://ind.ie](http://ind.ie)) describes this issue as "trickle-down technology". The fact is, while the FLO movement has had great successes, it hasn't liberated the general public yet.

Snowdrift.coop envisions a long-term patronage system in which the general public supports FLO projects but also has more influence and direct involvement. Alongside our funding focus, we're working to build a community that invites more people to use, support, and help build FLO resources. Of course, we work to partner with existing initiatives that share this mission, such as [OpenHatch](http://openhatch.org).

### How does Snowdrift.coop work? How will be coordinated and/or governed?

Snowdrift.coop is a multistakeholder (aka solidarity) cooperative. Like all cooperatives, we are owned by the members, but while a worker co-op serves the workers' interests or a consumer co-op serves the consumers' interests, we include and balance the interests of all parties. For Snowdrift.coop, we propose three member classes: the core developers who work on the site itself, the project teams who work on the independent projects that use the site, and the general community of project patrons.

Our proposed bylaws emphasize the best approaches to achieving consensus from all stakeholders. Each member class will have its own representation on the Board, elections will use score voting, and Board decisions will use a facilitated consensus process to ensure that all concerns are addressed.

### How would you compare Snowdrift.coop to venture capitalism? Kickstarter and Indiegogo?

Venture capital relies heavily on proprietary restrictions, and the mission of Snowdrift.coop is to provide a mechanism to fund projects *without* those restrictions. If Snowdrift.coop is successful, the general public will have better FLO resources and can reject proprietary offerings without compromising the quality of the products they use.

Kickstarter and other crowdfunding campaign sites are good for one-off campaigns. They partly address the snowdrift dilemma because each donor is part of achieving a collective goal. However, effective campaigns are quite costly and do not provide a *sustainable* solution. Most unfortunately, the majority of crowdfunded projects remain proprietary. Still, Snowdrift.coop works alongside these sites. They help kickstart or fund major initiatives. We're working to provide reliable long-term salaries to project teams.

We discuss these and other existing funding mechanisms at [https://snowdrift.coop/p/snowdrift/w/en/existingmechanisms](https://snowdrift.coop/p/snowdrift/w/en/existingmechanisms) and crowdfunding specifically at [https://snowdrift.coop/p/snowdrift/w/en/threshold-systems](https://snowdrift.coop/p/snowdrift/w/en/threshold-systems).

By the way, we actually reviewed every fundraising site we could find (over 700+ sites) to understand the overall market. For a summary of that research along with our thoughts and recommendations see [https://snowdrift.coop/p/snowdrift/w/en/othercrowdfunding](https://snowdrift.coop/p/snowdrift/w/en/othercrowdfunding). We found a few noteworthy sites: for example, we recommend Goteo.org as the most public-benefit, FLO-focused platform and give honorable mention to the lowest-fee and also FLO site Open.Tilt.com. We also acknowledge Gratipay.com as a decent FLO site for ongoing donations, although their model is just a classic unilateral, regular donation and so doesn't address the snowdrift dilemma. After all our research, it's clear that Snowdrift.coop truly represents a departure from any existing models.

### The site's FAQ says that the project isn't just for free software. Is there a danger of spreading yourself too thin?

As a music teacher, my first interest is better community-focused funding for FLO cultural and educational works. Also, as I mentioned above, we're focusing on helping FLO serve the general public. If we restricted the site to software, we would only reinforce our already programmer-centric community, and that would define a lot of our direction going forward.

As a cooperative, it is essential that we involve the full scope of stakeholders. So, we care about involving artists, teachers, students, researchers, journalists, and the general public from the beginning. That way, the co-op will develop to serve all the concerns of all these groups. Diverse perspectives offer immense value to any community, and the software world (and FLO software world in particular) struggles with that issue. Right now, at our start, is the best time for us to make sure we're including a wide-array of projects and people.

It's actually more common that people complain about how we've *limited* our scope to FLO non-rival digital products. People like our ideas and want to use the system to fund public goods like a local park or community center. Of course, they can use our code freely, but maybe in the long-term we'll expand to all forms of public goods. For now, we indeed want to avoid spreading ourselves too thin. We have a clear, moderate focus: those projects that mainly produce digital files that everyone can share online.

### How will people participate in the project?

Today, the core places to participate are: backend development in Haskell (we use the Yesod web framework); front-end design work; legal assistance; and outreach to partners, projects, and general supporters. Financial support could make a huge difference, although we recognize that potential donors face the snowdrift dilemma in choosing to fund us today.

We also welcome everyone who wants to test the site with fake money and otherwise play with things in our alpha state. Once the site is prepared for launch, we'll also need people to participate in the co-op governance, register projects, and pledge their support as patrons to their favorite projects.

Check out the site and consider filling out our volunteer-interest-form. We also have on-site discussion and wiki system, an IRC channel at freenode.net #snowdrift and the code is at both Gitorious (which is a FLO platform) and GitHub. We continue building additional tools to help people interact with us, and everything we use will be immediately available to any project on the site as well.

We will soon make it possible for select projects to sign up and test the alpha site, so anyone interested should contact us to be included.

### What stage are you at now? What milestones remain before you launch?

We need to make sure our handling of funds is legally clear. We have a decent amount of site development to finish, and we need to make sure the co-op structure is legally sound and the tools to run it are in place.

The immediate milestones for site development involve project sign-up, finalizing user account settings, and improving the overall site design and presentation. We also need to implement internationalization and do translations, but some of that could wait until after launching.

At this point, we're hoping to launch by early 2015.

### Is there anything else you would like to say about the project?

Sure, but I'm concerned that this interview is quite long already. We have lots more information on the site, and I look forward to discussing questions and feedback with the readers. I'm honored to have had the privilege to discuss all of this with you!
