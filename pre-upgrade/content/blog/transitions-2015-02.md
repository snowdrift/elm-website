---
title: "Transitions: Social, Legal, Technical, Personal"
slug: transitions-2015-02
date_published: 2015-02-13T07:45:00.000Z
date_updated: 2017-11-10T22:06:41.000Z
excerpt: Compared to all the activity surrounding our end-of-2014 fund drive ,things may seem a little stagnant around here, but appearances can be deceiving. We're in the midst of several transitions that will take Snowdrift.coop to a new state and much closer to launching.
---

Compared to all the activity surrounding our end-of-2014 [fund drive](https://snowdrift.tilt.com/), things may seem a little stagnant around here, but appearances can be deceiving. We're in the midst of several transitions that will take Snowdrift.coop to a new state and much closer to launching.

---

## Team transition

The biggest news: co-founder [David Thomas](https://wiki.snowdrift.coop/community/team/davidthomas) just started a new day-job, so he will have much less time to work on the site development. However, he has stepped up to fund a new full-time lead developer to take us through to our launch!

We're excited to announce that we've contracted with [Bryan Richter](https://wiki.snowdrift.coop/community/team/chreekat) to do this important work. We'll let Bryan introduce himself more in another post sometime after his official start next week.

Bryan will be supported by our ongoing contractor [Nikita Karetnikov](/https://wiki.snowdrift.coop/community/team/nkaretnikov) and the overall community of volunteers (of which I am just one of the volunteers, albiet the most active one). Notably, our fund-drive and our hiring process both brought in many new volunteers, some of whom have substantial technical skills and have offered to actively assist Bryan and provide more code reviewing.

## Tech transition

We're in the middle of updating our code to Yesod 1.4 and are testing the site with new build options via [Nix](https://nixos.org/nix/) and [Halcyon](https://halcyon.sh/). Our e-mail notifications system is being tested also and is nearly ready to go live. Finalizing and deploying these updates will be the first tasks for Bryan to handle as the new lead developer.

## Legal transition

We started work last week with [our new lawyer](http://www.esoplaw.com/) (thanks to the funding from the campaign), focusing on setting up the best governance and clarifying our status as a non-profit, multi-stakeholder cooperative.

Meanwhile, we're beginning the process of finding the best accountant(s) to help us with the details, legal and otherwise, of handling money (and while we have many great contacts to pursue, please let us know if you have any further advice about finding the best accountant — especially one who might support our mission enough to offer pro bono or discounted rates).

## Events

[![](/content/images/2017/04/attendee-webbadge-small.png)](http://www.socallinuxexpo.org/scale/13x)

We're also planning our involvement in several upcoming conferences and other [events](https://wiki.snowdrift.coop/community/events), the next being [SCALE](http://www.socallinuxexpo.org/scale/13x), in Los Angeles, CA, next week (February 20-22) where we'll have a dedicated booth for the first time.

I also just booked my flights and registered myself for [LibrePlanet 2015](https://libreplanet.org/2015/). We won't have a special booth or session, but I'll be there and doing my general constant discussion and promotion of Snowdrift.coop stuff in between learning about everyone else's wonderful software-freedom projects and perspectives.

## Site design transition

We now have a student team in a web-design class at Southeast Community College in Nebraska who chose Snowdrift.coop as their design project! I'm excited to see what they come up with. They'll be bringing some great outside perspective. At the same time, we have some in-progress site-design features coming from elsewhere in our community.

## Personal transitions

![](/content/images/2017/04/Frank-the-dog.jpg) In the midst of this, I've been busy myself. My wife and I adopted a new dog recently, and I now have around 20 private music students (having built up [my teaching studio](http://wolftune.com) again from about nothing when I moved last August to Portland, Oregon).

So, I haven't been able to dedicate full-time to Snowdrift.coop. Although it might be nice if one day I can make a reliable living working on Snowdrift.coop, I like teaching as well, and my position being self-employed lets me be flexible. For example, I now teach a couple students at no charge while they volunteer some of their time to Snowdrift.coop!

In the end, I'm doing this because the world needs Snowdrift.coop. I believe in the Snowdrift.coop [mission](https://wiki.snowdrift.coop/about/mission). I'm less interested in whether or not I get to be the one funded to make it happen or keep it going. I've never felt motivated by the idea of getting to do something instead of someone else. I find such zero-sum games unappealing. I feel motivated to work on Snowdrift.coop and on Free/Libre/Open projects in general because my work wouldn't be done otherwise. I hope to truly add value to the world rather than simply fight to get my piece of existing value.

## Help out where you can

With everything going on now, we have many reasons for optimism but also lots of work to do. I hope everyone interested in [helping us](https://wiki.snowdrift.coop/community/how-to-help) can see that each volunteer can truly add value. It can be a boon just to have more people providing feedback, asking questions, and helping explain ideas to others.

Personally juggling all the various issues has been challenging. It would help me greatly to have others around to pick up dropped balls (or better, to catch them before they drop) so we can keep the flow going. More precisely, the more other people who know the details about what's going on, the less I have to take *full* responsibility for tracking every detail myself.

So, if you want to see Snowdrift.coop launch sooner and really succeed, please see what you can do to get involved. Right now, expect some mess and confusion during our immediate transitions, but we'll keep working to improve everything from the web design to the code to the organizational structure. Participating will keep getting easier into the future, but we could certainly use your help today. Thanks so much to all the (now hundreds) of people who have already engaged in various ways!

## More news to come

Stay tuned for lots more news coming up in all these different areas… Thanks for reading!
