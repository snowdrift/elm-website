---
title: Joining the OSI
slug: osi-partnership
date_published: 2015-11-16T20:20:00.000Z
date_updated: 2017-11-16T07:04:41.000Z
excerpt: We are now a formal affiliate member of the Open Source Initiative, and are also getting support for our prototype research and development as an OSI incubator project.
---

**We are now a formal [affiliate member](http://opensource.org/affiliates) of the [Open Source Initiative](http://opensource.org)** joining existing affiliates such as Debian, KDE, the Document Foundation, Wikimedia, and many other great projects and organizations. Furthermore, we are also getting support for our prototype research and development as an [OSI incubator project](http://wiki.opensource.org/bin/Projects/Open+Source+Initiative+Working+Groups#HSnowdrift.coopPrototyping).

That means they will assist us in various ways as we continue our research and development. Most substantially, as an OSI-sponsored project, **supporters who care about donating to us under 501(c)(3) terms can now do so via the OSI**. Primarily, this matters for those where a tax deduction is valuable or for institutions that only donate to 501(c)(3)'s, and, for individuals, some employers will match your donations under these terms. We really still need all the support we can get in order to launch, so please see our [donate page](https://snowdrift.coop/donate) for more information.[[1]](#fn1)

---

Special thanks to OSI President, Allison Randall for volunteering to be our formal sponsor and to Patrick Masson for encouraging and facilitating this partnership. Also thanks to the rest of the [OSI Board](http://opensource.org/docs/board-annotated), most of whom I've had the pleasure to get to know personally through various conferences and events over the past couple years.

---

## OSI and "Free/Libre/Open" values

As anyone reading articles here will notice,we regularly use the term "FLO" for "Free/Libre/Open". Early on, we wrote a concise article to explain the relevant issues: **[What Does *Free/Libre/Open* Mean?](free-libre-open)** In that article, we discuss why everyone should see these terms as *additive* rather than *competitive* and that no single term works adequately on its own (and anyone unfamiliar with these issues would do well to review that article, and perhaps even the relevant links there, to better understand the rest of this post).

In that article's explanation of "FLO", we referenced historic political debates in the software world between the OSI and the Free Software Foundation (FSF). So, I wanted to take the time to discuss this issue further now that Snowdrift.coop is an affiliate and incubator project of the OSI.

### Learning to make sense of the terminology and politics

Prior to 2012, I was a user of OS X with no programming experience. I first came across the concept of software freedom when I read the Preamble in the [GNU General Public License](https://www.gnu.org/copyleft/gpl.html), probably seeing it with Audacity or Musescore when I was researching software to recommend to my students. Later, a friend showed me a video of GNU and FSF founder Richard Stallman discussing copyright. I remember feeling deeply inspired by his clear dedication to living by ethical principles.[[2]](#fn2)

When I first switched to GNU/Linux, it was an overwhelming experience in many respects. The terms "free software" and "open source" were used all over, and I wasn't sure what to make of it all. I learned about all sorts of details bullshit about restrictions and licensing, such as why my GNU/Linux music playing programs [couldn't access the CD database I had used with iTunes](https://en.wikipedia.org/wiki/Gracenote_licensing_controversy).

It seemed obvious that anyone who knew about these things would be pissed off like I was, and I assumed most people who used GNU/Linux did so because they cared about these injustices. I was then *shocked* to discover in further discussions that lots of insiders with years of GNU/Linux experience didn't care much about software freedom or even understand the ideas!

Later, after moving to Portland, Oregon, I attended my first OSCON. There, I really came to understand the extent to which "Open Source" advocates could be oblivious to the issues of ethics and power and community. Many (most?) of the companies promoting their products in the OSCON expo hall produced proprietary products! When I talked to various conference attendees about the Snowdrift.coop mission, only a minority of people cared that much about the ethics (even though most everyone thought our concept sounded good regardless).

Having become sensitized to the "free" vs "open" debate, I remember feeling surprised to meet folks from the OSI at the FSF's annual conference, LibrePlanet. But the more I talked to people, the more I recognized the nuances. The OSI folks I met were all members and supporters of the FSF. They saw things as less of a philosophical divide and more as just different organizations having different tactics. Some argue, reasonably enough, that getting people into the practical side of "open source" development serves as a gateway to then introduce them to the political issues of software freedom.

Over time, I came to understand the complexity of the different semantics and philosophies. I encountered people who follow Richard Stallman's semantics so strictly that they see "free software" and "open source" as *badges* in an absolute us-vs-them dichotomy. In contrast, I met many others who care deeply about the ethical and political issues but use the term "open source" for all sorts of practical reasons. Still, Richard Stallman is *not* creating straw men when he talks of people using "open source" to undermine the message of software freedom — I have met many people who certainly fit that description. The OSI was indeed founded with an explicit mission to appeal to corporate interests by *avoiding* the political messages of the free software movement.

Thankfully, the language and political alignments of 2015 are not those of 1999. The OSI *today*, with its Board of Directors full of FSF members, has replaced earlier divisive statements with [more aligned and nuanced views](http://opensource.org/faq#free-software) of their relation to "free software". In fact, one of their other recent projects focused on embracing "free/libre/open" as part of an [educational initiative](http://osi.xwiki.com/bin/Projects/About+FLOW+and+RENT+Relationships).

Now, the OSI's support of Snowdrift.coop will help our mission to free the commons and sustain a world without artificial restrictions on creative works.

We look forward to formalizing further partnerships with many other organizations as we proceed with our development. We welcome all forms of connections from aligned institutions and individuals. Please contact us about partnering, volunteering, donating, or with any questions or feedback. Snowdrift.coop is a community effort built on widespread cooperation.

---

1. 
We are still studying the [complex legal issues](https://wiki.snowdrift.coop/legal) around our own potential 501 status for our eventual full operations. The OSI partnership is specific to our prototyping and research stage.] [↩︎](#fnref1)

2. 
Incidentally, I remember telling my friend, "oh, that name, 'Richard Stallman', sounds familiar… I emailed him a couple years ago with some feedback for an article he wrote about copyright." I had no idea who he was at the time, but, as a musician, I had been thinking about and researching copyright issues a lot. I looked up my old email from May, 2010.

Basically, Richard had mocked the absurd implication that retroactive and extremely long copyright extensions helped encourage new creative works. I wrote him to say that, while I stood with him in opposing long copyright terms, I saw a logical argument about how long copyright could help new works: *New* works compete for attention with older works, and so restricting the older works under the same copyright terms as new works puts them on the same level. For example, a singing group might decide it was worth dealing with copyright hassles in order to sing songs written since 1928, and when they decide that, they might include quite new songs among their selections. If copyright were only 15 years long, then performers would more likely choose to skip anything copyrighted since they could avoid all the copyright costs and hassle and still not be relegated to very old music. Thus, long copyright terms could be helping support new authorship in this way. Of course, I acknowledge that the value of giving the public unrestricted access to a more robust public domain outweighed the benefit this might have in terms of supporting new work. Certainly, new authorship itself would benefit from access to a richer public domain (after all, all art is derivative). I was just pointing out that there *is* a basis by which long copyright terms can benefit today's artists.

Richard had written back and said that regardless of how much my point applied in some cases, the copyright-maximalist publishing industry wouldn't publicly argue that the purpose of long copyright terms was to  'sabotage' the public domain, and he felt no need to acknowledge or address such an argument unless it was seriously being made by copyright advocates.

In the end, my first interaction was typical of my experiences with Richard since. We sometimes disagree about tactics and details in subtle ways (as in the issue related to this blog post: I disagree with him over the *extent* to which he rejects the term "open source", even though I think his concerns there have *some* merit), but I respect Richard greatly and agree with his philosophies overall. Furthermore, he's always shown a talent for understanding what others are saying and considering it fairly. His wording about copyright as 'sabotage' of the public domain is a perfect description, and that helped me think about and talk about the issue more clearly.
[↩︎](#fnref2)
