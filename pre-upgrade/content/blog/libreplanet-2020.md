---
title: LibrePlanet 2020 — Gone Virtual
slug: libreplanet-2020
date_published: 2020-03-09T02:25:00.000Z
date_updated: 2020-03-15T20:03:18.000Z
tags: Conferences
excerpt: We had planned to have a Birds of a Feather (in-person meetup) at LibrePlanet. Like the conference itself, we'll be moving it online to our IRC channel, #snowdrift on freenode.net (also bridged to Matrix at #snowdrift:matrix.org).
---

![](/content/images/2020/03/logo.svg)
We'll be attending LibrePlanet virtually this year (along with everyone else, since the in-person component has been cancelled due to concerns over the COVID-19 outbreak).

We had planned to have a Birds of a Feather (in-person meetup) at LibrePlanet. Like the conference itself, we'll be moving it online to [our IRC channel](https://wiki.snowdrift.coop/community/irc), `#snowdrift` on freenode.net (also bridged to Matrix at `#snowdrift:matrix.org`). We're still nailing down the exact time (pending the new LibrePlanet schedule), but it will likely be Sunday afternoon, as we'd originally planned. We'll update this post when we know! It will take place at [18:15 Boston Time](https://www.timeanddate.com/worldclock/usa/boston), immediately after the conference concludes.

There were also two talks scheduled to be delivered by Snowdrift.coop-affiliated persons (although not directly *about* Snowdrift):

- 
[Bicycles as a metaphor for free software](https://libreplanet.org/2020/speakers/#2686) by Salt, our community director

- 
[Platform cooperativism, surveillance capitalism, predictive analysis, and you](https://libreplanet.org/2020/speakers/#2721) by Micky Metts, a member of our board of directors

Keep an eye on the [LibrePlanet schedule](https://libreplanet.org/2020/program/) for updates :)

## Status Update & Call for Volunteers

We're still working toward the Snowdrift.coop launch — when you can actually pledge to projects and help fund them.

- Snowdrift.coop itself as the first project has initial pledging active now, but no payments have yet been processed.
- Right now, one roadblock snowdrift we're focusing on clearing: fixing some UX issues and updated site front-end. **We already have updated designs, but we need more frontend developers to help implement them.**
- Otherwise, we're working on solidifying our governance and legal status and on initial outreach to potential early adopter projects to include in our initial full launch.

**Our main focus "at" LibrePlanet this year will be recruiting volunteers (particularly frontend developers but not exclusively).** Those with less time can still help by engaging and encouraging the overall community at [our forum](http://community.snowdrift.coop).

Come meet us and ask questions at [LibrePlanet (online)](https://libreplanet.org/2020/live/). Otherwise, keep up with hand washing and stay well everyone!
