---
title: Greetings from the New Lead Developer
slug: greetings-from-new-lead-dev
date_published: 2015-04-03T01:55:00.000Z
date_updated: 2017-11-10T22:04:02.000Z
excerpt: Hi everyone! It's been about seven weeks since I became the new lead developer for Snowdrift.coop, and I'm happy for this opportunity to help make this important project a reality. In this post, I'll introduce myself and talk about how I hope to move the project forward.
---

Hi everyone! It's been about seven weeks since I became the new lead developer for Snowdrift.coop, and I'm happy for this opportunity to help make this important project a reality. In this post, I'll introduce myself and talk about how I hope to move the project forward. I'll start with my professional and personal background before moving to the state of Snowdrift.coop.

---

I have been involved in a number of startups, usually as a software engineer. I have seen businesses at many different points of their lifespan, and have worked with both successes and failures. As one would imagine, I have worn many hats.

I would not call myself an entrepreneur, however. I am most interested in creating and fostering better software. Thus, I appreciate Snowdrift.coop for furthering [Free/Libre/Open](https://wiki.snowdrift.coop/about/free-libre-open) (FLO) software and for using Haskell. In my experiences, which I will describe briefly, these initiatives both contribute to a world with better software.

I first started using GNU/Linux in 2000. At that time, I preferred it simply for the user interface and the flexibility. Later, I came to understand how free software principles had allowed such a nice system to be available on my personal computer. As I learned more, I became involved in my local [Linux user's group](http://lugod.org/), and I became aware of the history and the interesting place in society that FLO occupies. Linux is still my first choice purely for usability, but I also think FLO has enormous importance in our society. I'm happy with the prospect of computers running the world — as long as the source code is high quality and freely available!

Haskell is a natural place for anyone interested in getting the most out of software. It is where a lot of research is done on improving the programmer/computer interface, and the Haskell community is full of smart, passionate people who work to improve themselves and others.

I picked up Haskell in 2010, while traveling in Scotland. I grew up in California, but I have been a bit of a wanderer the last five years. I rode my bicycle across Europe, went from zero-to-crappy in a couple spoken languages, learned Cyrillic, sailed on a tallship, hitchhiked, and made some good friends in far-away places. I never left software, though. I gained my Haskell experience by working on a number of personal and contract projects throughout this period.

I was still traveling, in fact, when I heard of Snowdrift.coop's need for a new lead developer. Coincidentally, I was back in the same town in the Scottish highlands where I had first started learning Haskell!

Now I have settled in Portland, Oregon, near co-founder Aaron Wolf, to get to work. And what is that work, you ask?

## State of Snowdrift.coop

I see three main areas of work needed on the project: legal, technical, and partnerships. Within my technical area, there are three sub-areas: operations, feature development, and developer infrastructure.

In my short time with the project so far, I have made strides in two areas: operations and developer infrastructure. Improvements include a [documented deploy process](https://git.snowdrift.coop/sd/snowdrift/blob/master/BUILD.md), suggestions for the coding style guide, research into different deployment possibilities, and implementation of a branch-management strategy. I have also simply taken over the many manual processes involved in handling patches, doing deploys and backups, and looking into operational issues. There is a *lot* of work still to be done on the operations front.

Theoretically, my top priority is implementing the missing pieces of the funding mechanism. My progress in that area has not been publicly visible so far. I have been reading all the many wiki pages that touch on the subject, taking notes, and organizing them into a good-enough comprehension of the principles. I've finished with that now, and plan to start implementing some of the missing features as soon as I've finished this post.

Looking ahead, there is an incredible amount of work to be done on all three technical fronts (operations, feature development, and developer infrastructure). My primary guiding principle is quality, which is perhaps a future blog post all of itself. That's a roundabout way of saying I haven't clarified my thoughts on what "quality" means, precisely. For the moment, suffice to say it imposes certain characteristics on code, such as testability, modularity, and clarity. If Snowdrift is to be successful in the long term, these characteristics must be strengthened.

Before I end this post, I would like to say thanks to the contributors that I have interacted with in the last few weeks. There has been significant work done on new website styling, improved navigation, and various bug fixes. It's always great to work with contributors old and new! Come join us on [irc](https://wiki.snowdrift.coop/community/irc) if you want to pitch in. Now that you know a little about me, feel free to come say hi.
