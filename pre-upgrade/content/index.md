---
title: New Snowdrift
type: page
---
![Snowdrift.coop](/images/wordmarks/snowdrift/with-tagline.png)

![Video here](https://sdproto.gitlab.io/img/index/video-poster.png)
######  Sign Up




# Funding free/libre/open projects

We support works that everyone can use, adapt, and share freely. But as public goods, anyone can freeride. And that presents a dilemma in getting funded. Our innovative platform brings people together to address this problem. Test adding some text.

# Matching and sustainability

In a single pledge, crowdmatching combines the two most effective solutions for getting voluntary donations to public goods:

- matching donations where patrons agree to give together and

- sustaining memberships which provide reliable, long-term salaries and accountability.

# Democracy

We don't serve the concentrated power of advertisers, investors, or wealthy philanthropists. Both in our technical design and in running as a non-profit co-op, we work to empower regular citizens. By coordinating the resources of large numbers of patrons, we can ensure that the creative work we fund serves the public interest.
