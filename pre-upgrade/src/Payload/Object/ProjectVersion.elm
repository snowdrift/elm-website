-- Do not manually edit this file, it was auto-generated by dillonkearns/elm-graphql
-- https://github.com/dillonkearns/elm-graphql


module Payload.Object.ProjectVersion exposing (..)

import Graphql.Internal.Builder.Argument as Argument exposing (Argument)
import Graphql.Internal.Builder.Object as Object
import Graphql.Internal.Encode as Encode exposing (Value)
import Graphql.Operation exposing (RootMutation, RootQuery, RootSubscription)
import Graphql.OptionalArgument exposing (OptionalArgument(..))
import Graphql.SelectionSet exposing (SelectionSet)
import Json.Decode as Decode
import Payload.InputObject
import Payload.Interface
import Payload.Object
import Payload.Scalar
import Payload.ScalarCodecs
import Payload.Union


parent :
    SelectionSet decodesTo Payload.Object.Project
    -> SelectionSet (Maybe decodesTo) Payload.Object.ProjectVersion
parent object____ =
    Object.selectionForCompositeField "parent" [] object____ (Basics.identity >> Decode.nullable)


version :
    SelectionSet decodesTo Payload.Object.ProjectVersion_undefined
    -> SelectionSet (Maybe decodesTo) Payload.Object.ProjectVersion
version object____ =
    Object.selectionForCompositeField "version" [] object____ (Basics.identity >> Decode.nullable)


id : SelectionSet (Maybe String) Payload.Object.ProjectVersion
id =
    Object.selectionForField "(Maybe String)" "id" [] (Decode.string |> Decode.nullable)


createdAt : SelectionSet (Maybe Payload.ScalarCodecs.DateTime) Payload.Object.ProjectVersion
createdAt =
    Object.selectionForField "(Maybe ScalarCodecs.DateTime)" "createdAt" [] (Payload.ScalarCodecs.codecs |> Payload.Scalar.unwrapCodecs |> .codecDateTime |> .decoder |> Decode.nullable)


updatedAt : SelectionSet (Maybe Payload.ScalarCodecs.DateTime) Payload.Object.ProjectVersion
updatedAt =
    Object.selectionForField "(Maybe ScalarCodecs.DateTime)" "updatedAt" [] (Payload.ScalarCodecs.codecs |> Payload.Scalar.unwrapCodecs |> .codecDateTime |> .decoder |> Decode.nullable)
