-- Do not manually edit this file, it was auto-generated by dillonkearns/elm-graphql
-- https://github.com/dillonkearns/elm-graphql


module Payload.Object.UserReadAccess exposing (..)

import Graphql.Internal.Builder.Argument as Argument exposing (Argument)
import Graphql.Internal.Builder.Object as Object
import Graphql.Internal.Encode as Encode exposing (Value)
import Graphql.Operation exposing (RootMutation, RootQuery, RootSubscription)
import Graphql.OptionalArgument exposing (OptionalArgument(..))
import Graphql.SelectionSet exposing (SelectionSet)
import Json.Decode as Decode
import Payload.InputObject
import Payload.Interface
import Payload.Object
import Payload.Scalar
import Payload.ScalarCodecs
import Payload.Union


permission : SelectionSet Bool Payload.Object.UserReadAccess
permission =
    Object.selectionForField "Bool" "permission" [] Decode.bool


where_ : SelectionSet (Maybe Payload.ScalarCodecs.JSONObject) Payload.Object.UserReadAccess
where_ =
    Object.selectionForField "(Maybe ScalarCodecs.JSONObject)" "where" [] (Payload.ScalarCodecs.codecs |> Payload.Scalar.unwrapCodecs |> .codecJSONObject |> .decoder |> Decode.nullable)
