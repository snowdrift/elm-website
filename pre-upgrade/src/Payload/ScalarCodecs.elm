-- Do not manually edit this file, it was auto-generated by dillonkearns/elm-graphql
-- https://github.com/dillonkearns/elm-graphql


module Payload.ScalarCodecs exposing (..)

import Json.Decode as Decode exposing (Decoder)
import Payload.Scalar exposing (defaultCodecs)


type alias DateTime =
    Payload.Scalar.DateTime


type alias EmailAddress =
    Payload.Scalar.EmailAddress


type alias JSONObject =
    Payload.Scalar.JSONObject


type alias Json =
    Payload.Scalar.Json


codecs : Payload.Scalar.Codecs DateTime EmailAddress JSONObject Json
codecs =
    Payload.Scalar.defineCodecs
        { codecDateTime = defaultCodecs.codecDateTime
        , codecEmailAddress = defaultCodecs.codecEmailAddress
        , codecJSONObject = defaultCodecs.codecJSONObject
        , codecJson = defaultCodecs.codecJson
        }
