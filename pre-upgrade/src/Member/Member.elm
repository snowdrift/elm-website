module Member.Member exposing (Member, memberNoEmail)


type alias Member =
    { loginEmail : Email
    , name : String
    }

memberNoEmail : String -> Member
memberNoEmail name =
    { loginEmail = Email ""
    , name = name
    }


-- TODO find a nice premade type and validator


type Email
    = Email String


