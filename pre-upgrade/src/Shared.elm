module Shared exposing (Data, Model, Msg(..), SharedMsg(..), signupButtonLearnMore, specialRoundedCorners, template)

{-| This is where your site-wide layout goes. The Shared.view function receives the view from a Page module, and can render it within a layout.

Must expose

    init : SharedTemplate Msg Model StaticData msg
    Msg - global Msgs across the whole app, like toggling a menu in the shared header view
    Model - shared state that persists between page navigations. This Shared.Model can be accessed by Page Modules.

-}

import Browser.Navigation
import Css as RawCss exposing (important, rem)
import Css.Global
import DataSource
import Delay
import Html as PlainHtml
import Html.Attributes as PlainHtmlAttributes
import Html.Styled as StyledHtml exposing (..)
import Html.Styled.Attributes as Attr exposing (..)
import Html.Styled.Attributes.Extra as Attr
import Html.Styled.Events as Events exposing (onClick, onSubmit)
import Html.Styled.Events.Extra as Events
import Login
import Member.Member as Member exposing (Member)
import Pages.Flags
import Pages.PageUrl exposing (PageUrl)
import Path exposing (Path)
import Process
import Remote.Errors
import Remote.Response exposing (GraphqlHttpResponse)
import Route exposing (Route)
import SharedTemplate exposing (SharedTemplate)
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Utilities as Tw exposing (..)
import Task
import Time
import View exposing (View)


template : SharedTemplate Msg (Maybe AppModel) Data msg
template =
    { init = init
    , update = updateIfRunning
    , view = view
    , data = data
    , subscriptions = subscriptions
    , onPageChange = Just OnPageChange
    }


{-| helper function by adroit that intercepts the normal update function so that it can already assume the app is running.
-}
updateIfRunning : Msg -> Maybe AppModel -> ( Maybe AppModel, Cmd Msg )
updateIfRunning msg maybeAppModel =
    -- lets our `update` function always assume the model is there.
    case ( maybeAppModel, msg ) of
        ( Just runningModel, _ ) ->
            let
                ( updatedModel, cmds ) =
                    update msg runningModel
            in
            ( Just updatedModel, cmds )

        ( Nothing, UpdateTime newTime ) ->
            let
                ( updatedModel, cmds ) =
                    update msg (startingModel newTime)
            in
            ( Just updatedModel, cmds )

        ( Nothing, _ ) ->
            -- the model being Nothing means we're not running in browser anyway, just static generating
            ( maybeAppModel, Cmd.none )


{-| This is the way our model starts.
-}
startingModel : Time.Posix -> AppModel
startingModel startTime =
    AppModel startTime Nothing defaultLoginDialog


{-| global Msgs across the whole app
-}
type Msg
    = OnPageChange
        { path : Path
        , query : Maybe String
        , fragment : Maybe String
        }
    | SharedMsg SharedMsg
    | UpdateTime Time.Posix
    | OpenLoginDialog
    | UpdateLoginDialog LoginDialog
    | AttemptLogin
    | Logout
    | RestServerResponse (Maybe String)
    | GraphServerResponse (GraphqlHttpResponse Login.LastLoginFailureDetails Login.LoggedInDetails)


type alias Data =
    ()


type SharedMsg
    = NoOp


{-| shared state that persists between page navigations. This Shared.Model can be accessed by Page Modules.

Dev note: here we wrap in "Maybe", so we can indicate if the app isn't running, but elm-pages expects our outermost type to be called Model and calls it from generated code, so we alias Model to Maybe AppModel. All of our own functions are designed to just work with AppModel then, assuming the app is running.

-}
type alias Model =
    Maybe AppModel


type alias AppModel =
    { time : Time.Posix
    , loggedInUser : Maybe Member.Member
    , loginDialog : LoginDialog
    }


init :
    Maybe Browser.Navigation.Key
    -> Pages.Flags.Flags
    ->
        Maybe
            { path :
                { path : Path
                , query : Maybe String
                , fragment : Maybe String
                }
            , metadata : route
            , pageUrl : Maybe PageUrl
            }
    -> ( Maybe AppModel, Cmd Msg )
init navigationKey flags maybePagePath =
    ( Nothing
    , Cmd.batch
        [ Task.perform UpdateTime Time.now
        , Login.makeGraphCheckLoginRequest GraphServerResponse
        ]
    )


update : Msg -> AppModel -> ( AppModel, Cmd Msg )
update msg model =
    case msg of
        OnPageChange _ ->
            ( model, Cmd.none )

        SharedMsg globalMsg ->
            ( model, Cmd.none )

        UpdateTime newTime ->
            ( { model | time = newTime }, Cmd.none )

        OpenLoginDialog ->
            let
                oldDialog =
                    model.loginDialog

                newDialog =
                    { oldDialog | open = True }
            in
            ( { model | loginDialog = newDialog }, Cmd.none )

        UpdateLoginDialog dialog ->
            ( { model | loginDialog = dialog }, Cmd.none )

        AttemptLogin ->
            let
                oldDialog =
                    model.loginDialog

                newDialog =
                    { oldDialog | status = LoginInProgress }

                loginGraph =
                    Login.makeGraphLoginRequest
                        { user = model.loginDialog.emailEntered
                        , pass = model.loginDialog.passphraseEntered
                        }
                        GraphServerResponse
            in
            ( { model | loginDialog = newDialog }
            , loginGraph
            )

        Logout ->
            let
                logout =
                    Login.logoutRequest
                        (\return -> UpdateLoginDialog (newDialog return))

                oldDialog =
                    model.loginDialog

                newDialog returnString =
                    case returnString of
                        "You have been logged out successfully." ->
                            { oldDialog | status = OnlineReadyToAttempt }

                        unusualResponse ->
                            { oldDialog | status = LastAttemptFailed <| Other <| "When attempting to log out, got unusual response: " ++ unusualResponse }

                -- TODO don't wipe login if logout was unsuccessful
            in
            ( { model | loggedInUser = Nothing }, logout )

        RestServerResponse maybeToken ->
            let
                oldDialog =
                    model.loginDialog

                newDialog =
                    { oldDialog | status = LastAttemptFailed (Other (Maybe.withDefault "No token recieved from REST request" maybeToken)) }
            in
            ( { model | loginDialog = newDialog }, Cmd.none )

        GraphServerResponse loginDetails ->
            let
                oldDialog =
                    model.loginDialog

                ( newDialog, loggedInUser ) =
                    case loginDetails of
                        Remote.Response.Success details ->
                            case Login.getLoggedInUser details of
                                Ok nowLoggedIn ->
                                    ( { oldDialog
                                        | status = LoginSuccess nowLoggedIn
                                        , open =
                                            if oldDialog.status == LoginSuccess nowLoggedIn then
                                                oldDialog.open

                                            else
                                                False
                                      }
                                    , Just (Member.memberNoEmail (Maybe.withDefault "nameless" nowLoggedIn.user.name))
                                    )

                                Err missing ->
                                    -- TODO deal with missing stuff if it's not all completely missing (which indicates logged out)
                                    ( { oldDialog | status = OnlineReadyToAttempt }, Nothing )

                        Remote.Response.Failure (Remote.Errors.Custom failDetails) ->
                            case failDetails.problem of
                                "The email or password provided is incorrect." ->
                                    ( { oldDialog | status = LastAttemptFailed <| BadCredentials oldDialog.emailEntered oldDialog.passphraseEntered }, Nothing )

                                unusualError ->
                                    ( { oldDialog | status = LastAttemptFailed <| Other unusualError }, Nothing )

                        Remote.Response.Failure problem ->
                            ( { oldDialog
                                | status = LastAttemptFailed <| Other <| "Failure. "

                                -- ++  (Debug.toString problem)
                              }
                            , Nothing
                            )
            in
            ( { model | loginDialog = newDialog, loggedInUser = loggedInUser }, Cmd.none )


subscriptions : Path -> Maybe AppModel -> Sub Msg
subscriptions _ _ =
    Sub.none


data : DataSource.DataSource Data
data =
    DataSource.succeed ()


view :
    Data
    ->
        { path : Path
        , route : Maybe Route
        }
    -> Maybe AppModel
    -> (Msg -> msg)
    -> View msg
    -> { body : PlainHtml.Html msg, title : String }
view sharedData page modelMaybe toMsg pageView =
    case pageView of
        View.PlainHtml html ->
            { body =
                toUnstyled <|
                    globalLayout sharedData
                        page
                        modelMaybe
                        toMsg
                        [ StyledHtml.fromUnstyled <| PlainHtml.div [ PlainHtmlAttributes.class "plain-html-page-body" ] html.body ]
            , title = html.title
            }

        View.ElmCSSStyledHtml styledHtml ->
            { body =
                toUnstyled <|
                    globalLayout sharedData
                        page
                        modelMaybe
                        toMsg
                        -- (StyledHtml.div [ StyledHtmlAttributes.class "styled-html-page-body" ] styledHtml.body)
                        styledHtml.body
            , title = styledHtml.title
            }


globalLayout :
    Data
    ->
        { path : Path
        , route : Maybe Route
        }
    -> Maybe AppModel
    -> (Msg -> msg)
    -> List (Html msg)
    -> Html msg
globalLayout sharedData page modelMaybe toMsg innerContents =
    let
        skipPagesWithoutFrame stuffList =
            if Path.toRelative page.path == "crowdmatch-org" then
            []
            else
            stuffList
    in
    div
        [ css
            (skipPagesWithoutFrame [ w_full
            , bg_white
            , bg_no_repeat
            , RawCss.backgroundSize2 (RawCss.pct 100) (RawCss.rem 40)
            , RawCss.backgroundImage <| RawCss.linearGradient2 (RawCss.deg 178) (RawCss.stop2 (RawCss.rgba 202 243 252 1) (RawCss.pct 0)) (RawCss.stop2 (RawCss.rgba 255 255 255 0) (RawCss.pct 50)) []
            , min_h_screen -- prevent background from being cut off on short pages
            ])
        ]
        ((skipPagesWithoutFrame [ globalStyles modelMaybe
         , StyledHtml.map toMsg <| header page.path
         ])
            ++ innerContents
            ++ (skipPagesWithoutFrame [ StyledHtml.map toMsg (loginOverlayIfRunning modelMaybe)
               , viewFooter
               ])
        )


globalStyles modelMaybe =
    let
        noScriptStyles =
            if modelMaybe == Nothing then
                --
                [ Css.Global.class "js-only" [ RawCss.display RawCss.none ] ]

            else
                []
    in
    Css.Global.global
        (Tw.globalStyles
            ++ [ Css.Global.html snowdriftFonts ]
            ++ noScriptStyles
        )


snowdriftFonts =
    [ RawCss.fontFamilies [ "Nunito" ], RawCss.color (RawCss.hex "#13628e") ]


header : Path -> Html Msg
header currentPath =
    StyledHtml.header
        [ css
            [ w_full
            , h_16
            , py_2
            , px_2
            , sticky
            , top_0
            , bg_white
            , bg_opacity_70
            , shadow_lg
            , backdrop_blur -- not working for some reason, even in chromium
            ]
        , class "backdrop_blur_fallback"
        , style "backdrop-filter" "blur(5px)"
        ]
        [ nav
            [ css
                [ m_auto -- so nav is centered in header
                , flex
                , space_x_8
                , max_w_3xl
                , h_full
                , text_color Theme.green_500
                , font_extrabold
                , text_2xl
                , items_center -- nav items vertically centered
                , justify_between
                ]
            ]
            [ mainLogo --add logo here
            , renderNavLink { url = "/how-it-works", label = "How It Works", newTabOpen = False }
            , renderNavLink { url = "/projects", label = "Projects", newTabOpen = False }
            , renderNavLink { url = "https://wiki.snowdrift.coop/about", label = "About", newTabOpen = False }
            , renderNavLink { url = "https://blog.snowdrift.coop/", label = "Blog", newTabOpen = False }
            , renderNavLink { url = "https://wiki.snowdrift.coop/", label = "Wiki", newTabOpen = False }
            , loginLink
            ]
        ]


mainLogo : Html msg
mainLogo =
    let
        logoImagePath =
            "/images/logos/snowdrift/dark-blue.svg"
    in
    a
        [ css [ h_full, inline_block, flex_none ]
        , id "main-logo"
        , href "/"
        , alt "Snowdrift"
        ]
        [ img
            [ src logoImagePath
            , css [ h_full, inline_block ]
            ]
            []
        ]



-- TODO can this still be done?
-- highlightableLink :
--     PagePath Pages.PathKey
--     -> Directory Pages.PathKey Directory.WithIndex
--     -> String
--     -> Html msg
-- highlightableLink currentPath linkDirectory displayName =
--     let
--         isHighlighted =
--             currentPath |> Directory.includes linkDirectory
--     in
--     a
--         [ css
--             (if isHighlighted then
--                 [ underline, text_blue_600, flex_initial ]
--
--              else
--                 []
--             )
--         , href (linkDirectory |> Directory.indexPath |> PagePath.toString)
--         ]
--         [ text displayName ]


type alias NavLink =
    { url : String
    , label : String
    , newTabOpen : Bool
    }



-- gitlabRepoLink : Html msg
-- gitlabRepoLink =
--     a []
--         { url = "https://gitlab.com/snowdrift/elm-website/"
--         , label =
--             Element.image
--                 [ Element.width (Element.px 22)
--                 , Font.color Palette.color.primary
--                 ]
--                 { src = ImagePath.toString Pages.images.logos.gitlab.blackOutline, description = "Gitlab repo" }
--         }
-- elmDocsLink : Html msg
-- elmDocsLink =
--     Element.newTabLink []
--         { url = "https://package.elm-lang.org/packages/dillonkearns/elm-pages/latest/"
--         , label =
--             Element.image
--                 [ Element.width (Element.px 22)
--                 , Font.color Palette.color.primary
--                 ]
--                 { src = ImagePath.toString Pages.images.elmLogo, description = "Elm Package Docs" }
--         }


type alias LoginDialog =
    { open : Bool
    , emailEntered : String
    , passphraseEntered : String
    , status : ServerLoginStatus
    }


type ServerLoginStatus
    = Offline
    | OnlineReadyToAttempt
    | LastAttemptFailed FailureReason
    | LoginInProgress
    | LoginSuccess { token : String, user : Login.User, expires : Int }


type FailureReason
    = Other String
    | BadCredentials String String


defaultLoginDialog : LoginDialog
defaultLoginDialog =
    LoginDialog False "" "" Offline


loginLink : Html Msg
loginLink =
    a
        [ href "#login"
        , onClick OpenLoginDialog
        ]
        [ text "Login" ]


renderNavLink : NavLink -> Html msg
renderNavLink navLink =
    a
        [ href navLink.url -- TODO new tab open
        ]
        [ text navLink.label ]


loginOverlayIfRunning : Maybe AppModel -> Html Msg
loginOverlayIfRunning modelMaybe =
    case modelMaybe of
        Just appModel ->
            case appModel.loggedInUser of
                Nothing ->
                    loginDialog appModel.loginDialog

                Just memberLoggedIn ->
                    alreadyLoggedInDialog appModel memberLoggedIn

        Nothing ->
            text ""


loginDialog : LoginDialog -> Html Msg
loginDialog dialog =
    let
        cardStyle =
            [ bg_white
            , p_6
            , border_gray_200
            , rounded_md
            , border_2
            , grid
            , grid_cols_1
            , space_y_6
            ]

        inputStyle =
            List.map important
                -- !important, since the css sanitize uses input[type=specific] selectors which win the specificity war over our classes
                [ border_gray_100
                , border_2
                , p_2
                , rounded_sm
                , placeholder_gray_400
                , text_2xl
                , font_light
                , text_blue_800 -- TODO use snowdrift blue
                ]

        visibleIfOpen =
            if dialog.open then
                visible

            else
                invisible

        disabledButtonStyles =
            [ bg_gray_500 ]

        statusStyles =
            case dialog.status of
                LastAttemptFailed (BadCredentials badEmail badPassphrase) ->
                    if dialog.emailEntered == badEmail && dialog.passphraseEntered == badPassphrase then
                        { errorMessage = "The credentials provided are incorrect."
                        , buttonText = "Changes needed"
                        , buttonStyles = disabledButtonStyles
                        }

                    else
                        { errorMessage = "The credentials previously provided were incorrect."
                        , buttonText = "Try again"
                        , buttonStyles = []
                        }

                LastAttemptFailed (Other error) ->
                    { errorMessage = error
                    , buttonText = "Try again"
                    , buttonStyles = []
                    }

                LoginSuccess { user } ->
                    { errorMessage = ""
                    , buttonText = "Success!"
                    , buttonStyles = []
                    }

                LoginInProgress ->
                    { errorMessage = ""
                    , buttonText = "Logging In..."
                    , buttonStyles = disabledButtonStyles
                    }

                OnlineReadyToAttempt ->
                    { errorMessage = ""
                    , buttonText = "Log in"
                    , buttonStyles = []
                    }

                Offline ->
                    { errorMessage = ""
                    , buttonText = "Offline"
                    , buttonStyles = disabledButtonStyles
                    }
    in
    section
        [ css
            [ fixed
            , top_0 -- TODO is there a combo class?
            , bottom_0
            , right_0
            , left_0
            , bg_blue_100
            , bg_opacity_50
            , grid
            , place_items_center
            , visibleIfOpen
            ]
        , style "backdrop-filter" "blur(12px)"
        , Events.onClickPreventDefaultAndStopPropagation (UpdateLoginDialog { dialog | open = False })
        ]
        [ node "dialog"
            [ css
                [ absolute
                , grid
                , space_y_8
                , bg_transparent -- override browser default "dialog" styles
                ]
            , Attr.attributeIf dialog.open (Attr.attribute "open" "")
            , Events.onClickPreventDefaultAndStopPropagation OpenLoginDialog
            ]
            [ img
                [ src "images/logos/snowdrift/dark-blue.svg"
                , css [ m_auto, w_24 ]
                ]
                []
            , h1
                [ css
                    [ text_center
                    , text_3xl
                    , font_thin
                    ]
                ]
                [ text "Log in to Snowdrift.coop" ]
            , div [ css cardStyle ]
                [ --label [ for "email" ] [ text "email" ]
                  input
                    [ id "email-input"
                    , type_ "email"
                    , placeholder "email"
                    , required True
                    , css inputStyle
                    , value dialog.emailEntered
                    , Events.onInput (\newEmail -> UpdateLoginDialog { dialog | emailEntered = newEmail })
                    , Events.onEnter AttemptLogin
                    ]
                    []

                -- , label [ for "passphrase" ] [ text "passphrase" ]
                , input
                    [ id "password-input"
                    , placeholder "passphrase"
                    , type_ "password"
                    , required True
                    , minlength 9
                    , css inputStyle
                    , value dialog.passphraseEntered
                    , Events.onInput (\newPass -> UpdateLoginDialog { dialog | passphraseEntered = newPass })
                    , Events.onEnter AttemptLogin
                    ]
                    []
                , div
                    [ css
                        [ bg_red_300
                        , border_2
                        , text_color Theme.white
                        , font_extrabold
                        , py_3
                        , px_28
                        , if statusStyles.errorMessage == "" then
                            Tw.hidden

                          else
                            inline_block
                        ]
                    ]
                    [ text statusStyles.errorMessage ]
                , button
                    [ type_ "submit"
                    , css
                        ([ bg_color Theme.green_500
                         , inline_block
                         , border_2
                         , shadow_md -- TODO color it rgba(19, 98, 142, 0.1)
                         , text_3xl
                         , text_color Theme.white
                         , font_extrabold
                         , py_3
                         , px_28 -- if text is long enough to not hit min width]
                         ]
                            ++ specialRoundedCorners
                            ++ statusStyles.buttonStyles
                        )
                    , class "js-only"
                    , onClick AttemptLogin
                    , onSubmit AttemptLogin
                    , Events.onEnter AttemptLogin
                    ]
                    [ text statusStyles.buttonText
                    ]
                ]
            , div [ css cardStyle ]
                [ text "Don't have an account? Sign Up!" ]
            ]
        ]


alreadyLoggedInDialog : AppModel -> Member -> Html Msg
alreadyLoggedInDialog model member =
    let
        dialog =
            model.loginDialog

        cardStyle =
            [ bg_white
            , p_6
            , border_gray_200
            , rounded_md
            , border_2
            , grid
            , grid_cols_1
            , space_y_6
            ]

        visibleIfOpen =
            if dialog.open then
                visible

            else
                invisible

        greeting =
            "Welcome to Snowdrift, " ++ member.name ++ "!"
    in
    section
        [ css
            [ fixed
            , top_0 -- TODO is there a combo class?
            , bottom_0
            , right_0
            , left_0
            , bg_blue_100
            , bg_opacity_50
            , grid
            , place_items_center
            , visibleIfOpen
            ]
        , style "backdrop-filter" "blur(12px)"
        , Events.onClickPreventDefaultAndStopPropagation (UpdateLoginDialog { dialog | open = False })
        ]
        [ node "dialog"
            [ css
                [ absolute
                , grid
                , space_y_8
                , bg_transparent -- override browser default "dialog" styles
                ]
            , Attr.attributeIf dialog.open (Attr.attribute "open" "")
            , Events.onClickPreventDefaultAndStopPropagation OpenLoginDialog
            ]
            [ img
                [ src "images/logos/snowdrift/dark-blue.svg"
                , css [ m_auto, w_24 ]
                ]
                []
            , h1
                [ css
                    [ text_center
                    , text_3xl
                    , font_thin
                    ]
                ]
                [ text greeting ]
            , div [ css cardStyle ]
                [ button
                    [ type_ "submit"
                    , css
                        ([ bg_color Theme.green_500
                         , inline_block
                         , border_2
                         , shadow_md -- TODO color it rgba(19, 98, 142, 0.1)
                         , text_3xl
                         , text_color Theme.white
                         , font_extrabold
                         , py_3
                         , px_28 -- if text is long enough to not hit min width]
                         ]
                            ++ specialRoundedCorners
                        )
                    , class "js-only"
                    , onClick Logout
                    ]
                    [ text "Log out" ]

                -- , div
                --     [ css
                --         ([ bg_red_300
                --          , border_2
                --          , text_color Theme.white
                --          , font_extrabold
                --          , py_3
                --          , px_28
                --          , if dialog.status == Nothing then Tw.hidden else inline_block
                --          ]
                --         )
                --     , onClick (UpdateLoginDialog {dialog | status = Nothing})
                --     ]
                --     [ text (Maybe.withDefault "" dialog.status) ]
                ]
            , div [ css cardStyle ]
                [ text "More login options coming soon." ]
            ]
        ]


specialRoundedCorners =
    let
        ( roundedValue1, roundedValue2 ) =
            ( rem 0.6, rem 3 )
    in
    -- TODO globalize this
    [ RawCss.borderBottomLeftRadius2 roundedValue1 roundedValue2
    , RawCss.borderTopLeftRadius2 roundedValue1 roundedValue2
    , RawCss.borderTopRightRadius2 roundedValue1 roundedValue2
    , RawCss.borderBottomRightRadius2 roundedValue1 roundedValue2
    ]


viewFooter : Html msg
viewFooter =
    footer
        [ css [ max_w_4xl, m_auto ] ]
        [ img [ src "images/wordmarks/snowdrift/with-tagline.png", css [ py_8, m_auto ] ] []
        , section [ class "footer-links", css [ grid, grid_cols_4, gap_1, text_2xl, text_color Theme.green_500, py_8 ] ]
            [ div [ css [ col_start_1, col_span_1 ] ]
                [ a [ css [ block ], href "/how-it-works" ] [ text "How It Works" ]
                , a [ css [ block ], href "https://blog.snowdrift.coop/" ] [ text "Blog" ]
                , a [ css [ block ], href "https://community.snowdrift.coop/" ] [ text "Forum" ]
                , a [ css [ block ], href "https://gitlab.com/snowdrift" ] [ text "Git" ]
                , a [ css [ block ], href "https://gitlab.com/snowdrift/elm-website" ] [ text "Site source code" ]
                ]
            , div [ css [ col_start_2, col_span_1 ] ]
                [ a [ css [ block ], href "https://community.snowdrift.coop/g/team" ] [ text "Team" ]
                , a [ css [ block ], href "https://sdproto.gitlab.io/donate/" ] [ text "Donate" ]
                , a [ css [ block ], href "https://sdproto.gitlab.io/sponsors" ] [ text "Our Sponsors" ]
                , a [ css [ block ], href "https://sdproto.gitlab.io/privacy/" ] [ text "Privacy Policy" ]
                , a [ css [ block ], href "https://sdproto.gitlab.io/terms/" ] [ text "Terms Of Service" ]
                ]
            , div [ css [ col_start_3, col_span_2 ] ]
                [ text "Our free/libre/open licenses: "
                , a [ href "https://creativecommons.org/licenses/by-sa/4.0" ] [ text "Creative Commons Attribution Share-Alike 4.0 International" ]
                , text " for content except "
                , a [ href "https://sdproto.gitlab.io/trademarks" ] [ text "trademarks" ]
                , text ", and "
                , a [ href "https://www.gnu.org/licenses/agpl" ] [ text "GNU AGPLv3+" ]
                , text " for code."
                ]
            ]
        ]


signupButtonLearnMore =
    section [ css [ m_auto, text_center, py_16 ] ]
        [ button
            [ css
                ([ bg_color Theme.green_500
                 , inline_block
                 , border_2
                 , shadow_md -- TODO color it rgba(19, 98, 142, 0.1)
                 , text_3xl
                 , text_color Theme.white
                 , font_extrabold
                 , py_3
                 , px_28 -- if text is long enough to not hit min width]
                 , RawCss.textShadow4 (rem 0.2) (rem 0.2) (rem 0) (RawCss.hex "3d955f") --  text shadow not yet supported by tailwind.
                 , bg_gradient_to_t
                 , from_green_600
                 , to_green_500
                 , transition_all
                 , ease_out
                 , duration_150
                 , RawCss.hover
                    [ RawCss.textShadow4 (rem 0) (rem 0) (rem 1) (RawCss.hsla 0 0 1 0.5)
                    , RawCss.transform (RawCss.translateY (rem 0.2)) -- tailwind class didn't seem to work
                    , shadow_none
                    ]
                 ]
                    ++ specialRoundedCorners
                )
            ]
            [ span
                [ css
                    []
                ]
                [ text "Sign Up" ]
            ]
        , a
            [ href ""
            , css [ py_4, text_3xl, text_color Theme.green_500, block, text_center ]
            ]
            [ text "learn more" ]
        ]
