module Page.Projects exposing (Data, Model, Msg, page)

import DataSource exposing (DataSource)
import Head
import Head.Seo as Seo
import Html.Styled as StyledHtml exposing (..)
import Html.Styled.Attributes as StyledHtmlAttributes exposing (..)
import Page exposing (Page, StaticPayload)
import Pages.PageUrl exposing (PageUrl)
import Pages.Url
import Shared
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Utilities as Tw exposing (..)
import View exposing (View)


type alias Model =
    ()


type alias Msg =
    Never


type alias RouteParams =
    {}


page : Page RouteParams Data
page =
    Page.single
        { head = head
        , data = data
        }
        |> Page.buildNoState { view = view }


data : DataSource Data
data =
    DataSource.succeed ()


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head static =
    Seo.summary
        { canonicalUrlOverride = Nothing
        , siteName = "Snowdrift"
        , image =
            { url = Pages.Url.external "images/logos/snowdrift/dark-blue-raster.png"
            , alt = "elm-pages logo"
            , dimensions = Nothing
            , mimeType = Nothing
            }
        , description = "TODO"
        , locale = Nothing
        , title = "Projects on Snowdrift" -- metadata.title -- TODO
        }
        |> Seo.website


type alias Data =
    ()


view :
    Maybe PageUrl
    -> Shared.Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel static =
    View.ElmCSSStyledHtml
        { title = "Projects on Snowdrift"
        , body =
            [ main_
                [ css
                    [ w_full
                    , py_16 -- move main content down from touching header
                    , grid_cols_12
                    , object_center
                    ]
                ]
                [ h1 [ css [ text_center, font_bold, text_4xl ] ] [ text "Projects" ]
                , projectExplainerSection
                , characterWithShovel
                , Shared.signupButtonLearnMore
                ]
            ]
        }


projectExplainerSection =
    section [ css [ m_auto ] ]
        [ div
            [ css [ m_auto, max_w_lg, pt_32, pb_16, text_xl, font_light ] ]
            [ text "As soon as possible, more projects will be listed here for support via Snowdrift.coop. After all, that's the whole point! For now, pledging to support Snowdrift.coop itself will help us get to that point. "
            ]
        , div [ css [ m_auto, my_1, px_8, py_8, bg_gray_100, max_w_xl, text_3xl, font_light ] ]
            [ a [ href "/snowdrift" ]
                [ img [ src "images/logos/snowdrift/dark-blue.svg", css [ h_8, inline ] ] []
                , span [ css [ px_4, font_bold ] ] [ text "Snowdrift.coop" ]
                ]
            ]
        , div [ css [ m_auto, my_1, px_8, py_8, bg_gray_100, max_w_xl, text_3xl, font_light ] ]
            [ h2 []
                [ span [ css [ text_2xl, italic, text_blue_300 ] ] [ text "... more coming soon" ]
                ]
            ]
        , div [ css [ m_auto, my_1, px_8, py_8, bg_gray_100, max_w_xl, text_3xl, font_light ] ]
            [ h2 []
                [ span [ css [ text_2xl, italic, text_blue_300 ] ] [ text "... no really, this is not just about us" ]
                ]
            ]
        ]


characterWithShovel =
    section [ css [ m_auto, py_16 ] ]
        [ img
            [ src "images/characters/excited_with_shovel.png"
            , css [ max_w_sm, m_auto ]
            ]
            []
        ]
