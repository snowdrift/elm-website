module Page.Index exposing (Data, Model, Msg, page)

import Css as RawCss exposing (important, rem, url)
import DataSource exposing (DataSource)
import Head
import Head.Seo as Seo
import Html.Styled as StyledHtml exposing (..)
import Html.Styled.Attributes as StyledHtmlAttributes exposing (..)
import Page exposing (Page, StaticPayload)
import Pages.PageUrl exposing (PageUrl)
import Pages.Url
import Shared
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Utilities as Tw exposing (..)
import View exposing (View)


type alias Model =
    ()


type alias Msg =
    Never


type alias RouteParams =
    {}


page : Page RouteParams Data
page =
    Page.single
        { head = head
        , data = data
        }
        |> Page.buildNoState { view = view }


data : DataSource Data
data =
    DataSource.succeed ()


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head static =
    Seo.summary
        { canonicalUrlOverride = Nothing
        , siteName = "Snowdrift"
        , image =
            { url = Pages.Url.external "TODO"
            , alt = "elm-pages logo"
            , dimensions = Nothing
            , mimeType = Nothing
            }
        , description = "TODO"
        , locale = Nothing
        , title = "Snowdrift: A new way to fund FLO projects" -- metadata.title -- TODO
        }
        |> Seo.website


type alias Data =
    ()


view :
    Maybe PageUrl
    -> Shared.Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel static =
    View.ElmCSSStyledHtml
        { title = "Home page"
        , body =
            [ main_
                [ css
                    [ w_full
                    , py_16 -- move main content down from touching header
                    , grid_cols_12
                    , object_center
                    , RawCss.backgroundImage (url "images/backgrounds/snow-home.png")
                    , bg_no_repeat
                    , bg_top
                    , bg_contain
                    ]
                ]
                [ img [ src "images/wordmarks/snowdrift/with-tagline.png", css [ py_8, m_auto ] ] []
                , section [ css [ py_8 ] ] [ introVideo ]
                , section [ css [ m_auto, max_w_4xl ] ] reasons
                , Shared.signupButtonLearnMore
                ]
            ]
        }


introVideo =
    video
        [ css [ max_w_3xl, m_auto, rounded ]
        , class "video"
        , controls True
        , attribute "disablepictureinpicture" "" -- get rid of FF floating button, short vid anyway
        , loop False
        , preload "auto"
        , poster "https://snowdrift.coop/static/img/home/video-poster.png?etag=HRsOQsI6"
        ]
        [ source [ src "https://archive.org/download/snowdrift-dot-coop-intro/snowdrift-dot-coop-intro.mp4", type_ "video/mp4" ]
            []
        , source [ src "https://archive.org/download/snowdrift-dot-coop-intro/snowdrift-dot-coop-intro.ogv", type_ "video/ogg" ]
            []
        , track [ kind "subtitles", attribute "label" "English", src "https://snowdrift.coop/static/snowdrift-intro-en.vtt", srclang "en" ]
            []
        , track [ kind "subtitles", attribute "label" "Français", src "https://snowdrift.coop/static/snowdrift-intro-fr.vtt", srclang "fr" ]
            []
        ]


reasons =
    [ h1 [ css [ m_auto, text_center, text_2xl, font_semibold, py_8 ] ]
        [ text "6 reasons why Snowdrift funding is better:" ]
    , ol [ css [ grid, grid_cols_2, gap_32, text_2xl, py_8 ] ]
        [ li [ css [ col_start_1, col_span_1 ] ]
            [ h2 [ css [ font_bold ] ]
                [ text "Crowdmatching" ]
            , span [ css [ font_normal ] ]
                [ text "We always make sure you're in it together with everybody else -- 1:1." ]
            ]
        , li [ css [ col_start_2, col_span_1 ] ]
            [ h2 [ css [ font_bold ] ]
                [ text "Ongoing funding" ]
            , span [ css [ font_normal ] ]
                [ text "Our goal is to support reliable income, every month." ]
            ]
        , li [ css [ col_start_1, col_span_1 ] ]
            [ h2 [ css [ font_bold ] ]
                [ text "No fees!!!" ]
            , span [ css [ font_normal ] ]
                [ text "That's right. We take no cut. Feel free to support us, though." ]
            ]
        , li [ css [ col_start_2, col_span_1 ] ]
            [ h2 [ css [ font_bold ] ]
                [ text "Public goods only!!!!!!" ]
            , span [ css [ font_normal ] ]
                [ text "We don't support just any project. It should be open and abundant." ]
            ]
        , li [ css [ col_start_1, col_span_1 ] ]
            [ h2 [ css [ font_bold ] ]
                [ text "Non-profit" ]
            , span [ css [ font_normal ] ]
                [ text "No venture capital, no investors. Just having our goals in mind." ]
            ]
        , li [ css [ col_start_2, col_span_1 ] ]
            [ h2 [ css [ font_bold ] ]
                [ text "We are a cooperative" ]
            , span [ css [ font_normal ] ]
                [ text "As a member, you're welcome to steer our organization with us." ]
            ]
        ]
    ]


signupButtonLearnMore =
    [ button
        [ css
            ([ bg_color Theme.green_500
             , inline_block
             , border_2
             , shadow_md -- TODO color it rgba(19, 98, 142, 0.1)
             , text_3xl
             , text_color Theme.white
             , font_extrabold
             , py_3
             , px_28 -- if text is long enough to not hit min width]
             ]
                ++ Shared.specialRoundedCorners
            )
        ]
        [ text "Sign Up" ]
    , a
        [ href ""
        , css [ py_4, text_3xl, text_color Theme.green_500, block, text_center ]
        ]
        [ text "learn more" ]
    ]
