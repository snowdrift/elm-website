module Page.Project_ exposing (Data, Model, Msg, page)

import Css as RawCss exposing (important, rem, url)
import DataSource exposing (DataSource)
import Head
import Head.Seo as Seo
import Html.Styled as StyledHtml exposing (..)
import Html.Styled.Attributes as StyledHtmlAttributes exposing (..)
import Page exposing (Page, StaticPayload)
import Pages.PageUrl exposing (PageUrl)
import Pages.Url
import Shared
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Utilities as Tw exposing (..)
import View exposing (View)


type alias Model =
    ()


type alias Msg =
    Never


type alias RouteParams =
    { project : String }


page : Page RouteParams Data
page =
    Page.prerender
        { head = head
        , data = data
        , routes = routes
        }
        |> Page.buildNoState { view = view }


routes : DataSource.DataSource (List RouteParams)
routes =
    DataSource.succeed [ { project = "snowdrift" } ]


data : RouteParams -> DataSource Data
data { project } =
    DataSource.succeed ()


head :
    StaticPayload Data RouteParams
    -> List Head.Tag
head static =
    Seo.summary
        { canonicalUrlOverride = Nothing
        , siteName = "Snowdrift"
        , image =
            { url = Pages.Url.external "images/logos/snowdrift/dark-blue-raster.png"
            , alt = "elm-pages logo"
            , dimensions = Nothing
            , mimeType = Nothing
            }
        , description = "TODO"
        , locale = Nothing
        , title = "Projects on Snowdrift" -- metadata.title -- TODO
        }
        |> Seo.website


type alias Data =
    ()


view :
    Maybe PageUrl
    -> Shared.Model
    -> StaticPayload Data RouteParams
    -> View Msg
view maybeUrl sharedModel static =
    View.ElmCSSStyledHtml
        { title = "Projects on Snowdrift"
        , body =
            [ main_
                [ css
                    [ w_full
                    , py_16 -- move main content down from touching header
                    , grid_cols_12
                    , object_center
                    ]
                ]
                [ projectHeader
                , introVideo
                , becomeAPatronSection
                , div [ css [ text_center, font_bold, text_2xl, text_gray_100 ] ] [ text "anybody know where I can find the image that goes here" ]
                , tableOfContents
                , aboutProject
                , hr [ css [ max_w_2xl, m_auto, pt_16 ] ] []
                , projectUpdates
                ]
            ]
        }


projectHeader =
    section [ css [ text_center, font_bold, text_4xl, max_w_4xl, py_16, m_auto ] ]
        [ h1 [ css [ text_4xl ] ] [ text "Snowdrift.coop" ]
        , h2 [ css [ text_2xl ] ] [ text "Crowdmatching for public goods" ]
        ]


introVideo =
    video
        [ css [ max_w_3xl, m_auto, rounded ]
        , class "video"
        , controls True
        , attribute "disablepictureinpicture" "" -- get rid of FF floating button, short vid anyway
        , loop False
        , preload "auto"
        , poster "https://snowdrift.coop/static/img/home/video-poster.png?etag=HRsOQsI6"
        ]
        [ source [ src "https://archive.org/download/snowdrift-dot-coop-intro/snowdrift-dot-coop-intro.mp4", type_ "video/mp4" ]
            []
        , source [ src "https://archive.org/download/snowdrift-dot-coop-intro/snowdrift-dot-coop-intro.ogv", type_ "video/ogg" ]
            []
        , track [ kind "subtitles", attribute "label" "English", src "https://snowdrift.coop/static/snowdrift-intro-en.vtt", srclang "en" ]
            []
        , track [ kind "subtitles", attribute "label" "Français", src "https://snowdrift.coop/static/snowdrift-intro-fr.vtt", srclang "fr" ]
            []
        ]


becomeAPatronSection =
    section [ css [ m_auto, text_center, py_16 ] ]
        [ button
            [ css
                ([ bg_color Theme.green_500
                 , inline_block
                 , border_2
                 , shadow_md -- TODO color it rgba(19, 98, 142, 0.1)
                 , text_3xl
                 , text_yellow_100
                 , font_extrabold
                 , py_3
                 , px_8 -- if text is long enough to not hit min width]
                 , RawCss.textShadow4 (rem 0.2) (rem 0.2) (rem 0) (RawCss.hex "3d955f") --  text shadow not yet supported by tailwind.
                 , bg_gradient_to_t
                 , from_green_600
                 , to_green_500
                 , transition_all
                 , ease_out
                 , duration_150
                 , RawCss.hover
                    [ RawCss.textShadow4 (rem 0) (rem 0) (rem 1) (RawCss.hsla 0 0 1 0.5)
                    , RawCss.transform (RawCss.translateY (rem 0.2)) -- tailwind class didn't seem to work
                    , shadow_none
                    ]
                 ]
                    ++ Shared.specialRoundedCorners
                )
            ]
            [ span
                [ css
                    []
                ]
                [ text "👤 Become A Patron" ]
            ]
        , a
            [ href "/how-it-works"
            , css [ py_4, text_3xl, text_color Theme.green_500, block, text_center ]
            ]
            [ text "ℹ️ how it works" ]
        ]


tableOfContents =
    section
        [ css
            [ m_auto
            , text_center
            , py_16
            , text_2xl
            , text_color Theme.green_500
            , flex
            , gap_14
            , max_w_4xl
            , font_bold
            ]
        ]
        [ a [ href "#about" ] [ text "About" ], a [ href "#updates" ] [ text "Updates" ] ]


aboutProject =
    section [ css [ m_auto, max_w_3xl, font_thin ] ]
        [ h1 [ css [ text_4xl ] ] [ text "About Snowdrift.coop" ]
        , div [ css [ max_w_xl, text_xl, font_normal, py_8, m_auto ] ]
            [ p [] [ text "Snowdrift is a nonprofit cooperative run by an international team driven by a common goal:" ]
            , p [] [ text "“To dramatically improve the ability of ordinary people to fund public goods -- things like software, music, journalism, and research -- that everyone can use and share without limitations.” Our innovated crowdmatching approach is outlined in our overview video and How It Works page." ]
            ]
        ]


projectUpdates =
    section [ css [ m_auto, max_w_3xl, font_thin ] ]
        ([ h1 [ css [ text_4xl ] ] [ text "Updates" ]
         ]
            ++ List.map projectArticle demoArticles
        )


projectArticle { date, title, content } =
    article [ css [ max_w_3xl, text_xl, font_normal, py_8, m_auto ] ]
        [ div [ class "update-date", css [ text_blue_500 ] ] [ text date ]
        , h1 [ css [ m_auto, max_w_2xl, text_2xl, py_8, text_color Theme.green_500 ] ] [ text title ]
        , div [ css [ m_auto, max_w_2xl ] ] [ text content ]
        , div [ css [ m_auto, text_right, text_color Theme.green_500 ] ] [ text "read more" ]
        ]


demoArticles =
    [ { date = "2021-01-07"
      , title = "Snowdrift.coop applying to be a 2021 High Priority Free Software Project"
      , content = "Since 2005, the FSF has been maintaining a list of High Priority Projects and there is currently an open call for submissions. We feel that Snowdrift.coop would make an excellent addition! We also feel that there is missing a key area: Sustainability and Growth. Read this post for more details..."
      }
    ]
