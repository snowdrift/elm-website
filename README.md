# Elm-pages 3

Elm-pages v3 upgrade complete. Up next, Payload 2 upgrade.

# Styles
- Going for a [utility classes approach](https://adamwathan.me/css-utility-classes-and-separation-of-concerns/) and [atomic css](https://johnpolacek.github.io/the-case-for-atomic-css/). Decided [Tailwind](https://tailwindcss.com/) was the nicest system to use. We also skip the dependencies: no need to compile tailwind with Webpack as [this Elm package](https://package.elm-lang.org/packages/matheus23/elm-default-tailwind-modules/latest/) uses the Elm compiler to handle that instead. This works in conjunction with elm-css for type-safe styling.

