module Shared exposing (Data, Model, Msg(..), SharedMsg(..), template, bigGreenButton, imgSrc)

import BackendTask exposing (BackendTask)
import Effect exposing (Effect)
import FatalError exposing (FatalError)
import Html as UnstyledHtml
import Html.Styled as H exposing (..)
import Html.Styled.Events as HE
import Pages.Flags
import Pages.PageUrl exposing (PageUrl)
import UrlPath exposing (UrlPath)
import Route exposing (Route)
import SharedTemplate exposing (SharedTemplate)
import View exposing (View)
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Utilities as Tw exposing (..)
import Html.Styled.Attributes as HA exposing (..)
import Html.Styled.Attributes.Extra as Attr
import Html.Styled.Events as Events exposing (onClick, onSubmit)
import Html.Styled.Events.Extra as Events
import Css exposing (rem)
import Css.Global
import Tailwind.Theme
import Tailwind.Theme as Theme


template : SharedTemplate Msg Model Data msg
template =
    let
        convertViewToUnstyled : { body : List (Html msg), title : String } -> { body : List (UnstyledHtml.Html msg), title : String }
        convertViewToUnstyled styledBodyAndTitle =
            -- this interception lets us use styled Html everywhere in the site.
            { body = List.map H.toUnstyled styledBodyAndTitle.body 
            , title = styledBodyAndTitle.title
            }
    in
    { init = init
    , update = update
    , view = \sharedData page model toMsg pageView -> convertViewToUnstyled <| view sharedData page model toMsg pageView
    , data = data
    , subscriptions = subscriptions
    , onPageChange = Nothing
    }


type Msg
    = SharedMsg SharedMsg
    | MenuClicked


type alias Data =
    ()


type SharedMsg
    = NoOp


type alias Model =
    { showMenu : Bool
    }


init :
    Pages.Flags.Flags
    ->
        Maybe
            { path :
                { path : UrlPath
                , query : Maybe String
                , fragment : Maybe String
                }
            , metadata : route
            , pageUrl : Maybe PageUrl
            }
    -> ( Model, Effect Msg )
init flags maybePagePath =
    ( { showMenu = False }
    , Effect.none
    )


update : Msg -> Model -> ( Model, Effect Msg )
update msg model =
    case msg of
        SharedMsg globalMsg ->
            ( model, Effect.none )

        MenuClicked ->
            ( { model | showMenu = not model.showMenu }, Effect.none )


subscriptions : UrlPath -> Model -> Sub Msg
subscriptions _ _ =
    Sub.none


data : BackendTask FatalError Data
data =
    BackendTask.succeed ()


view :
    Data
    ->
        { path : UrlPath
        , route : Maybe Route
        }
    -> Model
    -> (Msg -> msg)
    -> View msg
    -> { body : List (Html msg), title : String }
view sharedData page model toMsg pageView =
    { body =
        [   Css.Global.global (Tw.globalStyles ++ [ Css.Global.html snowdriftFonts ])
        ,   H.main_
                [ css
                    [ w_full
                    , bg_color Theme.white
                    , bg_no_repeat
                    , Css.backgroundSize2 (Css.pct 100) (Css.rem 40)
                    , Css.backgroundImage <| Css.linearGradient2 (Css.deg 178) (Css.stop2 (Css.rgba 202 243 252 1) (Css.pct 0)) (Css.stop2 (Css.rgba 255 255 255 0) (Css.pct 50)) []
                    , min_h_screen -- prevent background from being cut off on short pages
                    ]
                ]
                [ H.map toMsg <| header page.route
                , H.node "page" [] pageView.body
                , viewFooter
                --, H.map toMsg <| loginOverlayIfRunning model
                ]        
        ]
    , title = pageView.title
    }

snowdriftFonts =
    [ Css.fontFamilies [ "Nunito" ], Css.color (Css.hex "#13628e") ]


{-| The Snowdrift Header that appears throughout the site.
-}
header : Maybe Route -> Html Msg
header routeMaybe =
    let
        navLink : NavLink -> Html msg
        navLink link =
            if (Maybe.map Route.toString routeMaybe)  == Just link.url then
                a [ href (link.url), css [font_extrabold] ] [ text link.label ]
            else
                a [ href (link.url) ] [ text link.label ]
    in
    H.header
        [ css
            [ w_full
            , h_16
            , py_2
            , px_2
            , sticky
            , top_0
            , bg_color Theme.white
            , bg_opacity_70
            , shadow_lg
            , backdrop_blur -- not working for some reason, even in chromium
            ]
        , class "backdrop_blur_fallback"
        , style "backdrop-filter" "blur(5px)"
        ]
        [ nav
            [ css
                [ m_auto -- so nav is centered in header
                , flex
                , space_x_8
                , max_w_3xl
                , h_full
                , text_color Theme.green_500
                , font_extrabold
                , text_2xl
                , items_center -- nav items vertically centered
                , justify_between
                ]
            ]
            [ mainLogo 
            , navLink { url = "/how-it-works", label = "How It Works" }
            , navLink { url = "/projects", label = "Projects" }
            , navLink { url = "https://wiki.snowdrift.coop/about", label = "About" }
            , navLink { url = "https://blog.snowdrift.coop/", label = "Blog" }
            , navLink { url = "https://wiki.snowdrift.coop/", label = "Wiki" }
            -- , loginLink
            ]
        ]


type alias NavLink =
    { url : String
    , label : String
    }




mainLogo : Html msg
mainLogo =
    let
        logoImagePath =
            "/images/logos/snowdrift/dark-blue.svg"
    in
    a
        [ css [ h_full, inline_block, flex_none ]
        , id "main-logo"
        , href "/"
        , alt "Snowdrift"
        ]
        [ img
            [ src logoImagePath
            , css [ h_full, inline_block ]
            ]
            []
        ]




{-| The snowdrift footer that appears throughout the site.
-}
viewFooter : Html msg
viewFooter =
    H.footer
        [ css [ max_w_4xl, m_auto ] ]
        [ img [ src "images/wordmarks/snowdrift/with-tagline.png", css [ py_8, m_auto ] ] []
        , section [ class "footer-links", css [ grid, grid_cols_4, gap_1, text_2xl, text_color Theme.green_500, py_8 ] ]
            [ div [ css [ col_start_1, col_span_1 ] ]
                [ a [ css [ block ], href "/how-it-works" ] [ text "How It Works" ]
                , a [ css [ block ], href "https://blog.snowdrift.coop/" ] [ text "Blog" ]
                , a [ css [ block ], href "https://community.snowdrift.coop/" ] [ text "Forum" ]
                , a [ css [ block ], href "https://gitlab.com/snowdrift" ] [ text "Git" ]
                , a [ css [ block ], href "https://gitlab.com/snowdrift/elm-website" ] [ text "Site source code" ]
                ]
            , div [ css [ col_start_2, col_span_1 ] ]
                [ a [ css [ block ], href "https://community.snowdrift.coop/g/team" ] [ text "Team" ]
                , a [ css [ block ], href "https://sdproto.gitlab.io/donate/" ] [ text "Donate" ]
                , a [ css [ block ], href "https://sdproto.gitlab.io/sponsors" ] [ text "Our Sponsors" ]
                , a [ css [ block ], href "https://sdproto.gitlab.io/privacy/" ] [ text "Privacy Policy" ]
                , a [ css [ block ], href "https://sdproto.gitlab.io/terms/" ] [ text "Terms Of Service" ]
                ]
            , div [ css [ col_start_3, col_span_2 ] ]
                [ text "Our free/libre/open licenses: "
                , a [ href "https://creativecommons.org/licenses/by-sa/4.0" ] [ text "Creative Commons Attribution Share-Alike 4.0 International" ]
                , text " for content except "
                , a [ href "https://sdproto.gitlab.io/trademarks" ] [ text "trademarks" ]
                , text ", and "
                , a [ href "https://www.gnu.org/licenses/agpl" ] [ text "GNU AGPLv3+" ]
                , text " for code."
                ]
            ]
        ]

bigGreenButton labelText =
    let
        ( roundedValue1, roundedValue2 ) =
                    ( rem 0.6, rem 3 )

        specialRoundedCorners =
            [ Css.borderBottomLeftRadius2 roundedValue1 roundedValue2
            , Css.borderTopLeftRadius2 roundedValue1 roundedValue2
            , Css.borderTopRightRadius2 roundedValue1 roundedValue2
            , Css.borderBottomRightRadius2 roundedValue1 roundedValue2
            ]
    in
    button
        [ css
            ([ bg_color Theme.green_500
             , inline_block
             , border_2
             , shadow_md -- TODO color it rgba(19, 98, 142, 0.1)
             , text_3xl
             , text_color Theme.white
             , font_extrabold
             , py_3
             , px_28 -- if text is long enough to not hit min width]
             ]
                ++ specialRoundedCorners
            )
        ]
        [ text labelText ]



-- HELPERS

{-| Helper to provide the "src" attribute for an image. Just provide the path as a list of strings, one for each folder and the last being the filename. -}
imgSrc : List String -> H.Attribute msg
imgSrc pathList =
    HA.src (UrlPath.toRelative (UrlPath.join pathList))