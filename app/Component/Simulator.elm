module Component.Simulator exposing (..)

import Axis
import Browser
import Color exposing (Color, rgb255)
import Color.Oklch exposing (oklch, toColor)
import Dict exposing (Dict)
import Float.Extra
import Html exposing (Html, button, div, p, text)
import Html.Attributes as HA exposing (disabled, style)
import Html.Events exposing (onClick)
import Round exposing (roundCom)
import Scale exposing (BandConfig, BandScale, ContinuousScale, defaultBandConfig)
import Scale.Color
import Shape exposing (StackConfig, StackResult)
import TypedSvg exposing (g, rect, svg, text_)
import TypedSvg.Attributes exposing (attributeName, class, fill, fontFamily, fontSize, fontWeight, title, transform, viewBox)
import TypedSvg.Attributes.InPx exposing (height, width, x, y)
import TypedSvg.Core exposing (Svg, attribute)
import TypedSvg.Types exposing (Paint(..), Transform(..), px)
import W.Button
import W.Container
import W.Styles
import W.Table
import W.Tooltip



-- This is a component, meant to be embedded elsewhere in a page using these functions
-- main : Program () Model Msg
-- main =
--     Browser.sandbox
--         { init = initialModel
--         , view = view
--         , update = update
--         }


type alias Month =
    Int


type alias Dollars =
    Float


type alias PatronID =
    Int


type alias Model =
    { patrons : Dict PatronID Patron
    , currentMonth : Month
    , focusedMonth : Month
    , nextID : PatronID
    , highestSeenDonationsInAMonth : Dollars
    , highestSeenSinglePatronDonation : Dollars
    , mechanism : Mechanism
    , newPatronPledge : Dollars
    }


initialModel : Model
initialModel =
    { patrons = Dict.empty
    , currentMonth = 1
    , focusedMonth = 1
    , nextID = 1
    , highestSeenDonationsInAMonth = 0
    , highestSeenSinglePatronDonation = 0
    , mechanism = FixedMatchRatePerPatron { matchRate = 0.1 }
    , newPatronPledge = 10
    }


type alias Patron =
    { monthJoined : Month
    , monthDropped : Maybe Month
    , pledge : Dollars
    }


type Msg
    = AddNewPatron Month
    | DropPatron PatronID Month
    | RunCrowdmatch
    | Reset
    | FocusMonth Month


type Mechanism
    = FixedMatchRatePerPatron { matchRate : Dollars }
    | FixedMatchRatePercentageOfGoal { matchRate : Dollars, goal : Dollars }
    | VariablePledgePercentageOfGoal { goal : Dollars }
    | VariablePledgePercentageOfGoalWithRange { goalMin : Dollars, goalMax : Dollars }


update : Msg -> Model -> Model
update msg model =
    case msg of
        AddNewPatron month ->
            let
                newPledge =
                    case model.mechanism of
                        FixedMatchRatePerPatron { matchRate } ->
                            -- ignore pledge input, everyone must pledge the match rate
                            matchRate

                        _ ->
                            model.newPatronPledge

                newPatron =
                    { monthJoined = month
                    , monthDropped = Nothing
                    , pledge = newPledge
                    }
            in
            { model
                | patrons = Dict.insert model.nextID newPatron model.patrons
                , nextID = model.nextID + 1
                , highestSeenDonationsInAMonth = max model.highestSeenDonationsInAMonth (totalDonationsOnlyInMonth model model.currentMonth)
                , highestSeenSinglePatronDonation =
                    max model.highestSeenSinglePatronDonation (patronDonationInMonth model model.currentMonth model.nextID newPatron)
            }

        DropPatron patronID month ->
            { model
                | patrons =
                    Dict.update patronID (Maybe.map (\p -> { p | monthDropped = Just month })) model.patrons
            }

        RunCrowdmatch ->
            { model
                | currentMonth = model.currentMonth + 1
                , focusedMonth = model.currentMonth + 1
            }

        Reset ->
            initialModel

        FocusMonth newMonthToFocus ->
            { model
                | focusedMonth = newMonthToFocus
            }



-- MATH


activePatronsInMonth : Model -> Month -> Dict PatronID Patron
activePatronsInMonth model givenMonth =
    let
        patronJoinedGivenMonthOrEarlier patron =
            patron.monthJoined <= givenMonth

        patronDroppedLaterOrNever patron =
            case patron.monthDropped of
                Nothing ->
                    True

                Just monthDropped ->
                    monthDropped > givenMonth

        patronIsActive patron =
            patronJoinedGivenMonthOrEarlier patron && patronDroppedLaterOrNever patron
    in
    Dict.filter (\k p -> patronIsActive p) model.patrons


activePatronCountInMonth : Model -> Month -> Int
activePatronCountInMonth model month =
    Dict.size (activePatronsInMonth model month)


patronsJoinedInMonth : Model -> Month -> Dict PatronID Patron
patronsJoinedInMonth model givenMonth =
    let
        patronIsActive patron =
            case patron.monthDropped of
                Just monthDropped ->
                    patron.monthJoined == givenMonth && monthDropped > givenMonth

                Nothing ->
                    patron.monthJoined == givenMonth
    in
    Dict.filter (\k p -> patronIsActive p) model.patrons


countPatronsJoinedInMonth : Model -> Month -> Int
countPatronsJoinedInMonth model givenMonth =
    Dict.size (patronsJoinedInMonth model givenMonth)


patronDonationInMonth : Model -> Month -> PatronID -> Patron -> Dollars
patronDonationInMonth model givenMonth patronID patron =
    case model.mechanism of
        FixedMatchRatePerPatron { matchRate } ->
            matchRate * toFloat (activePatronCountInMonth model givenMonth)

        _ ->
            0 -- Debug.todo "other mechanisms"


totalDonationsOnlyInMonth : Model -> Month -> Dollars
totalDonationsOnlyInMonth model givenMonth =
    Dict.foldl
        (\patronID patron acc -> patronDonationInMonth model givenMonth patronID patron + acc)
        0
        (activePatronsInMonth model givenMonth)


totalDonationsEverAfterMonth : Model -> Month -> Dollars
totalDonationsEverAfterMonth model givenMonth =
    if givenMonth == 0 then
        0

    else
        totalDonationsOnlyInMonth model givenMonth + totalDonationsEverAfterMonth model (givenMonth - 1)


oldestPatronInMonth model givenMonth =
    Dict.keys (activePatronsInMonth model givenMonth)
        |> List.head
        |> Maybe.withDefault 0


countAllPatronsEver model =
    Dict.size model.patrons


getMatchRate : Model -> Float
getMatchRate model =
    case model.mechanism of
        FixedMatchRatePerPatron { matchRate } ->
            matchRate

        FixedMatchRatePercentageOfGoal { matchRate } ->
            matchRate

        _ ->
            0


dollarsToString : Dollars -> String
dollarsToString dollars =
    if dollars == 0 then
        "0"

    else if dollars < 1 then
        roundCom 0 (dollars * 100) ++ "¢"

    else
        "$" ++ roundCom 2 dollars



-- VIEW -----------------------------------------------------


view model =
    Html.node "page"
        [ HA.id "simulator" ]
        [ W.Styles.globalStyles
        , W.Styles.baseTheme
        , tempSpecialStyles
        , headerSection model
        , viewGraph model (Shape.stack (config model))
        , crowdmatchButtonsSection model
        , monthSection model
        , div [ style "text-align" "center" ]
            [ p [] [ text <| "Done with month " ++ String.fromInt model.currentMonth ++ "?" ]
            , p [] [ text <| "Run the crowdmatch!" ]
            ]
        ]


tempSpecialStyles =
    Html.node "style"
        []
        [ text
            """
        .hoverlabel {opacity: 0}
        .column:hover .hoverlabel {opacity:1;}
        """
        ] -- .bar {transition: height 1s;}


withTooltipBottom : String -> Html Msg -> Html Msg
withTooltipBottom message child =
    W.Tooltip.view [ W.Tooltip.bottom, W.Tooltip.fast ]
        { tooltip = [ text message ]
        , children = [ child ]
        }


withTooltipLeft : String -> Html Msg -> Html Msg
withTooltipLeft message child =
    W.Tooltip.view [ W.Tooltip.left, W.Tooltip.fast ]
        { tooltip = [ text message ]
        , children = [ child ]
        }


withTooltipRight : String -> Html Msg -> Html Msg
withTooltipRight message child =
    W.Tooltip.view [ W.Tooltip.right, W.Tooltip.fast ]
        { tooltip = [ text message ]
        , children = [ child ]
        }


headerSection : Model -> Html Msg
headerSection model =
    W.Container.view
        [ W.Container.horizontal
        , W.Container.spaceBetween
        , W.Container.node "section"
        , W.Container.alignCenterY
        , W.Container.padX_4
        , W.Container.padY_1
        ]
        [ Html.h1 [] [ text "Crowdmatch Simulator" ]
        , withTooltipLeft "Resets everything back to the beginning" <|
            W.Button.view [ W.Button.disabled (model == initialModel) ]
                { label = [ text "Start Over" ]
                , onClick = Reset
                }
        ]


monthSection : Model -> Html Msg
monthSection model =
    W.Container.view
        [ W.Container.horizontal
        , W.Container.spaceBetween
        , W.Container.node "section"
        , W.Container.alignCenterY
        , W.Container.pad_1
        ]
        [ withTooltipRight "Peek at the previous month" <|
            W.Button.view
                [ W.Button.disabled (model.focusedMonth == 1)
                , W.Button.outlined
                , W.Button.rounded
                ]
                { label = [ text "<" ]
                , onClick = FocusMonth (model.focusedMonth - 1)
                }
        , focusedMonthCard model
        , withTooltipLeft "Peek at the next month" <|
            W.Button.view
                [ W.Button.outlined
                , W.Button.rounded
                , W.Button.disabled (model.focusedMonth == model.currentMonth)
                ]
                { label = [ text ">" ]
                , onClick = FocusMonth (model.focusedMonth + 1)
                }
        ]


focusedMonthCard : Model -> Html Msg
focusedMonthCard model =
    W.Container.view
        [ W.Container.vertical
        , W.Container.spaceBetween
        , W.Container.node "section"
        , W.Container.card
        , W.Container.background "linear-gradient(#dbf6fe, #eefbfe)"
        , W.Container.alignCenterY
        , W.Container.alignCenterX
        , W.Container.padX_4
        , W.Container.padY_1
        ]
        [ Html.h2 []
            [ text <| "Month " ++ String.fromInt model.focusedMonth ]
        , W.Table.view
            [ W.Table.htmlAttrs [ style "background-color" "transparent", style "width" "auto" ] ]
            [ W.Table.column [] { label = "", content = \r -> text r.time }
            , W.Table.column []
                { label = "Patrons"
                , content = \r -> text (String.fromInt r.patrons)
                }
            , W.Table.column []
                { label = "Match Rate"
                , content = \r -> Html.span [] [ text (dollarsToString r.matchRate), Html.node "sub" [] [ text "/mo" ] ]
                }
            , W.Table.column [] { label = "Total $", content = \r -> text <| dollarsToString r.total }
            ]
            [ { time = "Before"
              , patrons = activePatronCountInMonth model (model.focusedMonth - 1)
              , matchRate = getMatchRate model
              , total = totalDonationsEverAfterMonth model (model.focusedMonth - 1)
              }
            , { time = "New"
              , patrons = countPatronsJoinedInMonth model model.focusedMonth
              , matchRate = getMatchRate model
              , total = totalDonationsOnlyInMonth model model.focusedMonth
              }
            , { time = "After"
              , patrons = activePatronCountInMonth model model.focusedMonth
              , matchRate = getMatchRate model
              , total = totalDonationsEverAfterMonth model model.focusedMonth
              }
            ]
        ]


crowdmatchButtonsSection : Model -> Html Msg
crowdmatchButtonsSection model =
    W.Container.view
        [ W.Container.horizontal
        , W.Container.spaceAround
        , W.Container.node "section"
        , W.Container.alignCenterY
        , W.Container.pad_1
        ]
        [ withTooltipBottom "Add a new patron this month" <|
            W.Button.view
                []
                { label = [ text "+1 Patron" ]
                , onClick = AddNewPatron model.focusedMonth
                }
        , withTooltipBottom "Drop the oldest patron" <|
            W.Button.view []
                { label = [ text "-1 Patron" ]
                , onClick = DropPatron (oldestPatronInMonth model model.currentMonth) model.currentMonth
                }
        , withTooltipBottom "Advance to the next month" <|
            W.Button.view
                [ if activePatronCountInMonth model model.currentMonth > 0 then
                    W.Button.primary

                  else
                    W.Button.secondary
                , W.Button.rounded
                , W.Button.disabled (activePatronCountInMonth model model.currentMonth == 0)
                ]
                { label = [ text "Crowdmatch! >" ]
                , onClick = RunCrowdmatch
                }
        ]



-- GRAPH --------------------------------------------------------------------


samplesForPatronBar : Model -> List ( String, List Float )
samplesForPatronBar model =
    let
        patronToSample ( patronID, patron ) =
            ( "Patron " ++ String.fromInt patronID
            , -- List.filter (\amt -> amt > 0)
              -- must be in overlap order
              [ patronDonationInMonth model model.currentMonth patronID patron
              , patron.pledge
              ]
            )
    in
    List.map patronToSample (Dict.toList model.patrons) -- |> Debug.log "samples"


w : Float
w =
    990


h : Float
h =
    504


padding : { bottom : Float, left : Float, right : Float, top : Float }
padding =
    { top = 30
    , left = 60
    , right = 30
    , bottom = 60
    }


config : Model -> StackConfig String
config model =
    { data = samplesForPatronBar model
    , offset = identity
    , order = identity
    }


reverseViridis : Float -> Color
reverseViridis progression =
    -- stylistic choice: the larger boxes look better in brighter colors, so invert the interpolator
    Scale.Color.viridisInterpolator (1 - progression)


colors : Int -> List Color
colors size =
    let
        colorScale =
            Scale.sequential reverseViridis ( 0, toFloat size - 1 )
                |> Scale.convert
    in
    List.range 0 (size - 1)
        |> List.map (colorScale << toFloat)


patronOkColor patronID =
    let
        hueDegrees =
            toFloat patronID + 200
    in
    { hue = Float.Extra.modBy 360 hueDegrees / 360
    , chroma = 0.2
    , lightness = 0.5
    , alpha = 1
    }


column : BandScale ( PatronID, Month ) -> ( ( Int, PatronID, Patron ), List ( Float, Float ) ) -> Svg Msg
column xScale ( ( patronIndex, patronID, patron ), values ) =
    let
        block barIndex ( upperY, lowerY ) =
            rect
                ([ x <| Scale.convert xScale ( patronIndex, patron.monthJoined )
                 , y <| lowerY
                 , width <| Scale.bandwidth xScale
                 , height <| (abs <| upperY - lowerY)

                 --  , TypedSvg.Attributes.origin "0"
                 , class [ "bar" ]
                 ]
                    ++ withColor barIndex
                )
                [-- TypedSvg.animate
                 -- [attributeName "height"
                 -- , TypedSvg.Attributes.from 0
                 -- , TypedSvg.Attributes.to (abs <| upperY - lowerY)
                 -- , TypedSvg.Attributes.dur (TypedSvg.Types.Duration "1s")
                 -- ] []
                ]

        basePatronColor =
            patronOkColor patronID

        withColor barIndex =
            if barIndex == 0 then
                [ fill (Paint (toColor basePatronColor)) ]

            else
                [ fill (Paint (toColor { basePatronColor | lightness = 0.6 }))
                ]

        labels =
            [ text_
                [ x <| Scale.convert xScale ( patronIndex, patron.monthJoined )
                , y 13
                , fontFamily [ "Helvetica", "sans-serif" ]
                , fontSize (px 10)
                , class [ "hoverlabel" ]
                ]
                [ text <| "p" ++ String.fromInt patronID ]
            , text_
                [ x <| Scale.convert xScale ( patronIndex, patron.monthJoined )
                , y 22
                , fontFamily [ "Helvetica", "sans-serif" ]
                , fontSize (px 10)
                , class [ "hoverlabel" ]
                ]
                [ text <| "m" ++ String.fromInt patron.monthJoined ]
            ]
    in
    g [ class [ "column" ] ]
        (List.indexedMap block values ++ labels)


viewGraph : Model -> StackResult String -> Html Msg
viewGraph model { values, labels, extent } =
    let
        -- The basis for constructing a stacked chart
        --   values: Sorted list of values, where every item is a (yLow, yHigh) pair.
        --  labels: Sorted list of labels
        --  extent: The minimum and maximum y-value. Convenient for creating scales.
        patronsWithIDsAndIndex =
            List.indexedMap (\n ( patronID, patron ) -> ( n, patronID, patron )) <| Dict.toList model.patrons

        patronIndexes =
            List.range

        patronXValues =
            List.indexedMap (\n patron -> ( n, patron.monthJoined )) <| Dict.values model.patrons

        patronMonthsOnly =
            List.map .monthJoined <| Dict.values model.patrons

        monthValues =
            values

        monthAxisTicks =
            List.range 1 (model.currentMonth + 1)
                |> List.map (\mo -> ( mo, mo ))

        xScale : BandScale ( Int, Month )
        xScale =
            let
                innerPadding =
                    if countAllPatronsEver model >= 100 then
                        -- bars are too close for a full pixel gap, get rid of it to prevent ugliness
                        0

                    else
                        0.1
            in
            Scale.band { align = 0, paddingInner = innerPadding, paddingOuter = 0.2 } ( 0, w - (padding.left + padding.right) ) patronXValues

        xScaleVisual : BandScale Month
        xScaleVisual =
            Scale.band { align = 0, paddingInner = 0.1, paddingOuter = 0.2 } ( 0, w - (padding.left + padding.right) ) patronMonthsOnly

        ( yDataMin, yDataMax ) =
            ( 0
              -- Tuple.first extent
            , --model.highestSeenSinglePatronDonation
              Tuple.second extent + 1 - Basics.logBase 100000 (Tuple.second extent)
            )

        yScale : ContinuousScale Float
        yScale =
            Scale.linear ( h - (padding.top + padding.bottom), 0 ) ( yDataMin, yDataMax )

        --|> Scale.nice 1
        formatMoneyScale =
            Axis.tickFormat (\dollars -> dollarsToString dollars)

        scaledValues =
            List.map (List.map (\( y1, y2 ) -> ( Scale.convert yScale y1, Scale.convert yScale y2 ))) monthValues
    in
    svg [ viewBox 0 0 w h ]
        [ g [ transform [ Translate (padding.left - 1) (h - padding.bottom) ] ]
            [ Axis.bottom [] (Scale.toRenderable (\month -> "m" ++ String.fromInt month) xScaleVisual) ]
        , g [ transform [ Translate (padding.left - 1) padding.top ] ]
            [ Axis.left [ formatMoneyScale ] yScale ]
        , g [ transform [ Translate padding.left padding.top ], class [ "series" ] ] <|
            List.map (column xScale) (List.map2 (\a b -> ( a, b )) patronsWithIDsAndIndex scaledValues)
        ]
