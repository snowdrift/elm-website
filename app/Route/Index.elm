module Route.Index exposing (ActionData, Data, Model, Msg, route)

import BackendTask exposing (BackendTask)
import Css exposing (important, rem, url)
import FatalError exposing (FatalError)
import Head
import Head.Seo as Seo
import Html.Styled as H exposing (..)
import Html.Styled.Attributes as HA exposing (..)
import Pages.Url
import PagesMsg exposing (PagesMsg)
import Route
import RouteBuilder exposing (App, StatelessRoute)
import Shared
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Theme as Theme
import Tailwind.Utilities as Tw exposing (..)
import UrlPath
import View exposing (View)


type alias Model =
    {}


type alias Msg =
    ()


type alias RouteParams =
    {}


type alias Data =
    { message : String
    }


type alias ActionData =
    {}


route : StatelessRoute RouteParams Data ActionData
route =
    RouteBuilder.single
        { head = head
        , data = data
        }
        |> RouteBuilder.buildNoState { view = view }


data : BackendTask FatalError Data
data =
    BackendTask.succeed Data
        |> BackendTask.andMap
            (BackendTask.succeed "Hello!")


head :
    App Data ActionData RouteParams
    -> List Head.Tag
head app =
    Seo.summary
        { canonicalUrlOverride = Nothing
        , siteName = "Snowdrift"
        , image =
            { url = [ "images", "logos", "snowdrift", "dark-blue.svg" ] |> UrlPath.join |> Pages.Url.fromPath
            , alt = "snowdrift logo"
            , dimensions = Nothing
            , mimeType = Nothing
            }
        , description = "Crowdmatching for public goods"
        , locale = Nothing
        , title = "Snowdrift: A new way to fund FLO projects"
        }
        |> Seo.website


view :
    App Data ActionData RouteParams
    -> Shared.Model
    -> View (PagesMsg Msg)
view app shared =
    { title = "Home page"
    , body =
        [ main_
            [ css
                [ w_full
                , py_16 -- move main content down from touching header
                , grid_cols_12
                , object_center
                , Css.backgroundImage (url "images/backgrounds/snow-home.png")
                , bg_no_repeat
                , bg_top
                , bg_contain
                ]
            ]
            [ img [ src "images/wordmarks/snowdrift/with-tagline.png", css [ py_8, m_auto ] ] []
            , section [ css [ py_8 ] ] [ introVideo ]
            , section [ css [ m_auto, max_w_4xl ] ] reasons
            , section [ css [ m_auto, text_center, py_16 ] ]
                [ Shared.bigGreenButton "Sign up"
                , a
                    [ href ""
                    , css [ py_4, text_3xl, text_color Theme.green_500, block, text_center ]
                    ]
                    [ text "learn more" ]
                ]
            ]
        ]
    }


introVideo =
    video
        [ css [ max_w_3xl, m_auto, rounded ]
        , class "video"
        , controls True
        , attribute "disablepictureinpicture" "" -- get rid of FF floating button, short vid anyway
        , loop False
        , preload "auto"
        , poster "https://snowdrift.coop/static/img/home/video-poster.png?etag=HRsOQsI6"
        ]
        [ source [ src "https://archive.org/download/snowdrift-dot-coop-intro/snowdrift-dot-coop-intro.mp4", type_ "video/mp4" ]
            []
        , source [ src "https://archive.org/download/snowdrift-dot-coop-intro/snowdrift-dot-coop-intro.ogv", type_ "video/ogg" ]
            []
        , track [ kind "subtitles", attribute "label" "English", src "https://snowdrift.coop/static/snowdrift-intro-en.vtt", srclang "en" ]
            []
        , track [ kind "subtitles", attribute "label" "Français", src "https://snowdrift.coop/static/snowdrift-intro-fr.vtt", srclang "fr" ]
            []
        ]


reasons =
    [ h1 [ css [ m_auto, text_center, text_2xl, font_semibold, py_8 ] ]
        [ text "6 reasons why Snowdrift funding is better:" ]
    , ol [ css [ grid, grid_cols_2, gap_32, text_2xl, py_8 ] ]
        [ li [ css [ col_start_1, col_span_1 ] ]
            [ h2 [ css [ font_bold ] ]
                [ text "Crowdmatching" ]
            , span [ css [ font_normal ] ]
                [ text "We always make sure you're in it together with everybody else -- 1:1." ]
            ]
        , li [ css [ col_start_2, col_span_1 ] ]
            [ h2 [ css [ font_bold ] ]
                [ text "Ongoing funding" ]
            , span [ css [ font_normal ] ]
                [ text "Our goal is to support reliable income, every month." ]
            ]
        , li [ css [ col_start_1, col_span_1 ] ]
            [ h2 [ css [ font_bold ] ]
                [ text "No fees!!!" ]
            , span [ css [ font_normal ] ]
                [ text "That's right. We take no cut. Feel free to support us, though." ]
            ]
        , li [ css [ col_start_2, col_span_1 ] ]
            [ h2 [ css [ font_bold ] ]
                [ text "Public goods only!!!!!!" ]
            , span [ css [ font_normal ] ]
                [ text "We don't support just any project. It should be open and abundant." ]
            ]
        , li [ css [ col_start_1, col_span_1 ] ]
            [ h2 [ css [ font_bold ] ]
                [ text "Non-profit" ]
            , span [ css [ font_normal ] ]
                [ text "No venture capital, no investors. Just having our goals in mind." ]
            ]
        , li [ css [ col_start_2, col_span_1 ] ]
            [ h2 [ css [ font_bold ] ]
                [ text "We are a cooperative" ]
            , span [ css [ font_normal ] ]
                [ text "As a member, you're welcome to steer our organization with us." ]
            ]
        ]
    ]
