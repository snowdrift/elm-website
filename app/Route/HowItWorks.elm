module Route.HowItWorks exposing (ActionData, Data, Model, Msg, RouteParams, route)

{-| 
@docs Model, Msg, RouteParams, route, Data, ActionData
-}

import FatalError exposing (FatalError)
import BackendTask exposing (BackendTask)
import Effect
import ErrorPage
import Pages.Url
import Css
import FatalError
import Head
import Html.Styled as H exposing (..)
import Html.Styled.Attributes as HA exposing (css)
import PagesMsg
import RouteBuilder
import Server.Request
import Server.Response
import Shared
import UrlPath
import View
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Utilities as Tw exposing (..)
import Tailwind.Theme as Theme

type alias Model =
    {}


type Msg
    = NoOp


type alias RouteParams =
    {}


route : RouteBuilder.StatefulRoute RouteParams Data ActionData Model Msg
route =
    RouteBuilder.single { data = data, head = head }
        |> RouteBuilder.buildWithLocalState
            { view = view
            , init = init
            , update = update
            , subscriptions = subscriptions
            }


init :
    RouteBuilder.App Data ActionData RouteParams
    -> Shared.Model
    -> ( Model, Effect.Effect Msg )
init app shared =
    ( {}, Effect.none )


update :
    RouteBuilder.App Data ActionData RouteParams
    -> Shared.Model
    -> Msg
    -> Model
    -> ( Model, Effect.Effect Msg )
update app shared msg model =
    case msg of
        NoOp ->
            ( model, Effect.none )


subscriptions :
    RouteParams -> UrlPath.UrlPath -> Shared.Model -> Model -> Sub Msg
subscriptions routeParams path shared model =
    Sub.none


type alias Data =
    {}


type alias ActionData =
    {}


data : BackendTask FatalError Data
data =
    BackendTask.succeed Data



head : RouteBuilder.App Data ActionData RouteParams -> List Head.Tag
head app =
    []


view :
    RouteBuilder.App Data ActionData RouteParams
    -> Shared.Model
    -> Model
    -> View.View (PagesMsg.PagesMsg Msg)
view app shared model =
    { title = "How Snowdrift.coop works"
    , body = 
        [ H.main_
            [ css
                [ w_full
                , py_16 -- move main content down from touching header
                , grid_cols_12
                , object_center
                ]
            ]
            [ h1 [ css [ text_center, font_bold, text_4xl ] ] [ text "How It Works" ]
            , explainerSection
            , section [ css [ m_auto, text_center, py_16 ] ]
                [ Shared.bigGreenButton "Sign up"
                , a
                    [ HA.href ""
                    , css [ py_4, text_3xl, text_color Theme.green_500, block, text_center ]
                    ]
                    [ text "learn more" ]
                ]
            ]
        ]
    }


explainerSection =
    section [ css [ m_auto ] ]
        [ img
            [ css [ m_auto, max_w_3xl, py_32 ]
            , [ "images", "explainers", "project_crowdmatching.png" ] |> UrlPath.join |> UrlPath.toRelative |> HA.src
            ]
            []
        , div [ css [ px_8, rounded_3xl, py_8, bg_color Theme.gray_100, m_auto, max_w_xl, text_2xl, font_light ] ]
            [ p [] [ text "Every patron matches every other." ]
            , p [] [ text "Projects receive their donations monthly. " ]
            ]
        ]



action :
    RouteParams
    -> Server.Request.Request
    -> BackendTask.BackendTask FatalError.FatalError (Server.Response.Response ActionData ErrorPage.ErrorPage)
action routeParams request =
    BackendTask.succeed (Server.Response.render {})