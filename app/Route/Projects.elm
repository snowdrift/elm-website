module Route.Projects exposing (ActionData, Data, Model, Msg, RouteParams, route)

{-| 
@docs Model, Msg, RouteParams, route, Data, ActionData
-}

import FatalError exposing (FatalError)
import BackendTask exposing (BackendTask)
import Effect
import ErrorPage
import Pages.Url
import Css
import FatalError
import Head
import Html.Styled as H exposing (..)
import Html.Styled.Attributes as HA exposing (css)
import PagesMsg
import RouteBuilder
import Server.Request
import Server.Response
import Shared
import UrlPath
import View
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Utilities as Tw exposing (..)
import Tailwind.Theme as Theme

type alias Model =
    {}


type Msg
    = NoOp


type alias RouteParams =
    {}


route : RouteBuilder.StatefulRoute RouteParams Data ActionData Model Msg
route =
    RouteBuilder.single { data = data, head = head }
        |> RouteBuilder.buildWithLocalState
            { view = view
            , init = init
            , update = update
            , subscriptions = subscriptions
            }


init :
    RouteBuilder.App Data ActionData RouteParams
    -> Shared.Model
    -> ( Model, Effect.Effect Msg )
init app shared =
    ( {}, Effect.none )


update :
    RouteBuilder.App Data ActionData RouteParams
    -> Shared.Model
    -> Msg
    -> Model
    -> ( Model, Effect.Effect Msg )
update app shared msg model =
    case msg of
        NoOp ->
            ( model, Effect.none )


subscriptions :
    RouteParams -> UrlPath.UrlPath -> Shared.Model -> Model -> Sub Msg
subscriptions routeParams path shared model =
    Sub.none


type alias Data =
    {}


type alias ActionData =
    {}


data : BackendTask FatalError Data
data =
    BackendTask.succeed Data



head : RouteBuilder.App Data ActionData RouteParams -> List Head.Tag
head app =
    []


view :
    RouteBuilder.App Data ActionData RouteParams
    -> Shared.Model
    -> Model
    -> View.View (PagesMsg.PagesMsg Msg)
view app shared model =
    { title = "Projects on Snowdrift"
    , body = 
        [ H.main_
            [ css
                [ w_full
                , py_16 -- move main content down from touching header
                , grid_cols_12
                , object_center
                ]
            ]
            [ h1 [ css [ text_center, font_bold, text_4xl ] ] [ text "Projects" ]
            , projectExplainerSection
            , characterWithShovel
            , section [ css [ m_auto, text_center, py_16 ] ]
                [ Shared.bigGreenButton "Sign up"
                , a
                    [ HA.href ""
                    , css [ py_4, text_3xl, text_color Theme.green_500, block, text_center ]
                    ]
                    [ text "learn more" ]
                ]
            ]
        ]
    }


projectExplainerSection =
    section [ css [ m_auto ] ]
        [ div
            [ css [ m_auto, max_w_lg, pt_32, pb_16, text_xl, font_light ] ]
            [ text "As soon as possible, more projects will be listed here for support via Snowdrift.coop. After all, that's the whole point! For now, pledging to support Snowdrift.coop itself will help us get to that point. "
            ]
        , div [ css [ m_auto, my_1, px_8, py_8, bg_color Theme.gray_100, max_w_xl, text_3xl, font_light ] ]
            [ a [ HA.href "/snowdrift" ]
                [ img [ Shared.imgSrc [ "images", "logos", "snowdrift", "dark-blue.svg" ], css [ h_8, inline ] ] []
                , span [ css [ px_4, font_bold ] ] [ text "Snowdrift.coop" ]
                ]
            ]
        , div [ css [ m_auto, my_1, px_8, py_8, bg_color Theme.gray_100, max_w_xl, text_3xl, font_light ] ]
            [ h2 []
                [ span [ css [ text_2xl, italic, text_color Theme.blue_300 ] ] [ text "... more coming soon" ]
                ]
            ]
        , div [ css [ m_auto, my_1, px_8, py_8, bg_color Theme.gray_100, max_w_xl, text_3xl, font_light ] ]
            [ h2 []
                [ span [ css [ text_2xl, italic, text_color Theme.blue_300 ] ] [ text "... no really, this is not just about us" ]
                ]
            ]
        ]


characterWithShovel =
    section [ css [ m_auto, py_16 ] ]
        [ img
            [ Shared.imgSrc [ "images", "characters", "excited_with_shovel.png" ] 
            , css [ max_w_sm, m_auto ]
            ]
            []
        ]


action :
    RouteParams
    -> Server.Request.Request
    -> BackendTask.BackendTask FatalError.FatalError (Server.Response.Response ActionData ErrorPage.ErrorPage)
action routeParams request =
    BackendTask.succeed (Server.Response.render {})