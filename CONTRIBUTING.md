This site is built using [Elm](https://elm-lang.org/), via the static site generator [Elm-Pages](https://elm-pages.com/). The project requires [Node](https://nodejs.org/) installed, though there is very little Javascript/Typescript involved.

Thank's to Elm's beginner-friendly error messages, you can quickly make edits to the site without learning Elm - use the recommended setup below. For style and layout, however, you will need to refer to the Tailwind documentation to look up which css classes to apply.

# How to contribute
You'll need Git, Node, and a text editor or IDE with Elm support. 

# Quick setup for beginners
## 1. Prerequisites
 [Install node and pnpm](https://nodejs.org/en) and git if you don't already have them. 
Ubuntu Example:
```
sudo apt install node pnpm git xclip
```

## 2. Install IDE
 Install [VSCodium](https://vscodium.com/) and [the Elm extension](https://open-vsx.org/extension/Elmtooling/elm-ls-vscode) if you don't already have them. If you use VSCode or VSCodium already, search for the "elm" extension.
Ubuntu Example:
```
sudo apt install vscodium
codium --install-extension Elmtooling.elm-ls-vscode
```

## 3. Set up Git with GitLab
 [Set up Git](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account) and [connect it to Gitlab](https://docs.gitlab.com/ee/user/ssh.html#see-if-you-have-an-existing-ssh-key-pair), if you haven't already.
Example:
```
git config --global user.name "your_gitlab_username"
git config --global user.email "your-gitlab-email@replaceme.com"
ssh-keygen -t ed25519 -C "your-gitlab-email@replaceme.com"
```
(enter x3)
```
xclip -sel clip < ~/.ssh/id_ed25519.pub
``````
([copies the key](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account) so you can paste it in your [account keys page](https://gitlab.com/-/profile/keys))

## 4. Download the source code
Clone the repository to your computer.
Example:
```
git clone git@gitlab.com:snowdrift/elm-website.git
```

## 5. Open in editor
Open the new repo folder using VSCodium (File -> Open Folder) and then a new Terminal (Terminal -> New Terminal).
In the terminal, run: 
```
npm install
```

## 6. Start the server
That's it! You can now run `npm start` and see your changes reflected live at http://localhost:1234/.

