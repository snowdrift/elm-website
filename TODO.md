# Next
- [ ] Get Project structure into Payload
- [ ] Logged in dialog: add section with expiry time and an "extend" button. (Auth Refresh command)
- [ ] Logged in dialog: add section with admin link, if User.canAccessAdmin is true
- [ ] Break out from Tailwind default modules into full tailwind (https://github.com/matheus23/elm-tailwind-modules)
- [ ] Login Dialog: once custom tailwind is installed, use "peer" classes and get input fields to show when invalid.

# Later
- [ ] Upgrade to elm-pages 3.0 when it comes out

# Done
- [X] Create this TODO file